
//# sourceURL=nda-transfer-plugin/ndaTransferConfig.js

if (typeof CCF === 'undefined') {
	CCF = { };
}

if (typeof CCF.ndatransferconfig === 'undefined') {
	CCF.ndatransferconfig = {
		currentProject:"",
		setupChecked: false,
		initialized: false
	 };
}

CCF.ndatransferconfig.whenInserted = function(selector, callback) {
  if ($(selector).length) {
    callback();
  } else {
    setTimeout(function() {
      CCF.ndatransferconfig.whenInserted(selector, callback);
    }, 100);
  }
};

CCF.ndatransferconfig.newPackage = function() {
	if (!CCF.ndatransferconfig.setupChecked) {
		CCF.ndatransferconfig.checkSetup();
	}
}

CCF.ndatransferconfig.checkSetup = function() {
	if (typeof CCF.pcpconfig === 'undefined' || typeof CCF.pcpconfig.currentData === 'undefined') {
		xmodal.message("Error", "The Pipeline Control Panel is required in order to enable NDA transfers");
		return;
	}
	CCF.ndatransferconfig.pcpsettings = {};
	$.ajax({
		type: "GET",
		url:serverRoot+'/xapi/pipelineControlPanelConfig/' + CCF.ndatransferconfig.currentProject + '/settings',
		cache: false,
		async: false,
		context: this,
		dataType: 'json'
	}).done( function(data, textStatus, jqXHR) {
		CCF.ndatransferconfig.pcpsettings = data;
	});
	$.ajax({
		type: "GET",
		url:serverRoot+'/xapi/ccfGuidConfigApi/' + CCF.ndatransferconfig.currentProject + '/configureRequiredFields',
		cache: false,
		async: false,
		context: this,
		dataType: 'json'
	}).done( function(data, textStatus, jqXHR) {
		CCF.ndatransferconfig.configureRequiredFields = data;
	});
	var transferPipelines = 0;
	CCF.ndatransferconfig.pcpsettings.hasNdaTransferPipeline = false;
	CCF.ndatransferconfig.pcpsettings.hasNdaGuidPipeline = false;
	for (var i=0; i<CCF.pcpconfig.currentData.length; i++) {
		var p = CCF.pcpconfig.currentData[i];
		if (typeof p.pipeline === 'undefined') {
			continue;
		}
		if (p.pipeline == "NdaTransferPipeline") {
			transferPipelines += 1;
			CCF.ndatransferconfig.pcpsettings.hasNdaTransferPipeline = true;
		} else if (p.pipeline == "NdaGuidPipeline") {
			transferPipelines += 1;
			CCF.ndatransferconfig.pcpsettings.hasNdaGuidPipeline = true;
		}
	}
	if (transferPipelines>=2 && CCF.ndatransferconfig.pcpsettings.pcpEnabled && CCF.ndatransferconfig.configureRequiredFields.fieldDefinitionComplete) {
		CCF.ndatransferconfig.packageInfoPanel(); 
		return;
	}
	CCF.ndatransferconfig.packageInfoPanel(); 
	xmodal.confirm({
		title:  "Setup Required",
		content:  "<em>Pipeline Control Panel</em> pipelines and settings along with other configuration is required to enable NDA transfer functionality.  A check reports that at least some required configuration is not complete." +
				"<br><br><b>Do you want the required configuration changes to be made now?</b>",
		width: 500,
		height: 250,
		okLabel:  "OK",
		okClose:  true,
		okAction:  function() {
			CCF.ndatransferconfig.pcpSetup();
		}
	});
}

CCF.ndatransferconfig.pcpSetup = function() {
	if (!CCF.ndatransferconfig.configureRequiredFields.fieldDefinitionComplete) {
		$.ajax({
			type: "POST",
			url:serverRoot+'/xapi/ccfGuidConfigApi/' + CCF.ndatransferconfig.currentProject + '/configureRequiredFields',
			cache: false,
			async: false,
			context: this,
			dataType: 'json'
		}).done( function(data, textStatus, jqXHR) {
			CCF.ndatransferconfig.configureRequiredFields = data;
			if (CCF.ndatransferconfig.configureRequiredFields.fieldDefinitionComplete) {
				XNAT.ui.banner.top(3000, 'Required field definitions have been enabled', 'success');
			}
		});
	}
	if (!CCF.ndatransferconfig.pcpsettings.pcpEnabled) {
		$("#pcp-enabled").click();
		$("#pcp-enabled").closest('.panel-body').siblings('.panel-footer').find('.save').click();
		CCF.ndatransferconfig.pcpsettings.pcpEnabled = true;
		XNAT.ui.banner.top(3000, 'The Pipeline Control Panel (required for the NDA transfer plugin) has been enabled.', 'success');
	}
	var addedTransfer = false;
	var addedGuid = false;
	if (!CCF.ndatransferconfig.pcpsettings.hasNdaTransferPipeline || !CCF.ndatransferconfig.pcpsettings.hasNdaGuidPipeline) {
		if (!CCF.ndatransferconfig.pcpsettings.hasNdaTransferPipeline) {
			var tp = '{"pipeline":"NdaTransferPipeline","description":"NDA Transfer Pipeline","updateFrequency":"Daily","selector":"org.nrg.ccf.ndatransfer.components.pcp.NdaTransferSelector","submitter":"org.nrg.ccf.ndatransfer.components.pcp.NdaTransferSubmitter","statusUpdater":"org.nrg.ccf.pcpcomponents.statusupdaters.SimpleStatusUpdater","prereqChecker":"org.nrg.ccf.ndatransfer.components.pcp.NdaTransferPrereqChecker","validator":"org.nrg.ccf.ndatransfer.components.pcp.NdaTransferValidator","execManager":"org.nrg.ccf.ndatransfer.components.pcp.NdaTransferExecManager","configurables":{}}';
			var parsedTP = JSON.parse(tp);
			if (parsedTP.pipeline !== 'undefined') {
				 CCF.pcpconfig.currentData.push(parsedTP);
			}
			addedTransfer = true;
		}
		if (!CCF.ndatransferconfig.pcpsettings.hasNdaGuidPipeline) {
			var tg = '{"pipeline":"NdaGuidPipeline","description":"NDA GUID Pipeline","updateFrequency":"Daily","selector":"org.nrg.ccf.pcpcomponents.selectors.AllSubjectsPipelineSelector","submitter":"org.nrg.ccf.pcpcomponents.submitters.SimpleManagerControlledSubmitter","statusUpdater":"org.nrg.ccf.pcpcomponents.statusupdaters.SimpleStatusUpdater","prereqChecker":"org.nrg.ccf.pcpcomponents.prereqcheckers.SimpleNoPrereqPrereqChecker","validator":"org.nrg.ccf.ndaguid.pcp.NdaGuidValidator","execManager":"org.nrg.ccf.ndaguid.pcp.NdaGuidExecManager","configurables":{}}';
			var parsedTG = JSON.parse(tg);
			if (parsedTG.pipeline !== 'undefined') {
				 CCF.pcpconfig.currentData.push(parsedTG);
			}
			addedGuid = true;
		}
	}
	if (addedTransfer || addedGuid) {
		CCF.pcpconfig.prevData = CCF.pcpconfig.currentData;
		CCF.pcpconfig.postDataUpdates('saved',false); 
		CCF.pcpconfig.populatePipelinesTable('#pipelines-list-container');
		if (addedTransfer) {
			CCF.pcpconfig.refreshPipelineCache('NdaTransferPipeline');
		}
		if (addedGuid) {
			CCF.pcpconfig.refreshPipelineCache('NdaGuidPipeline');
		}
	}

}

CCF.ndatransferconfig.displayPackagesTable = function(tag) {

	CCF.ndatransferconfig.currentProject = $(".project-id").html();
	CCF.ndatransferconfig.currentTag = tag;
	if (!CCF.ndatransferconfig.initialized) {
		CCF.ndatransferconfig.initializeValueLists();
		CCF.ndatransferconfig.initializeEntityConfigs();
		CCF.ndatransferconfig.initializePcpHelperInfo();
	}
	$.ajax({
		type: "GET",
		url:serverRoot+'/xapi/ccfNdaTransferConfig/' + CCF.ndatransferconfig.currentProject + '/dataPackages',
		cache: false,
		async: true,
		context: this,
		dataType: 'json'
	}).done( function(data, textStatus, jqXHR) {
		CCF.ndatransferconfig.currentData = data;
		if (!$.isArray(CCF.ndatransferconfig.currentData)) {
			$(CCF.ndatransferconfig.currentTag).html("ERROR:  The data package configuration data returned was invalid (PROBLEM=NOT_AN_ARRAY).");
			$("#create-new-package").hide();
			return;
		}
		CCF.ndatransferconfig.createPackagesTable();
	}).fail( function(data, textStatus, jqXHR) {
		$(CCF.ndatransferconfig.currentTag).html("ERROR:  Could not retrieve package configuration.");
		$("#create-new-package").hide();
	});

}

CCF.ndatransferconfig.initializeEntityConfigs = function() {

	if (typeof CCF.ndatransferconfig.entityConfigs === 'undefined') {
		$.ajax({
			type: "GET",
			url:serverRoot+'/xapi/ccfNdaTransferConfig/entityConfigs',
			cache: false,
			async: true,
			context: this,
			dataType: 'json'
		}).done( function(data, textStatus, jqXHR) {
			CCF.ndatransferconfig.entityConfigs = data;
		}).fail( function(data, textStatus, jqXHR) {
			console.log("ERROR:  Could not retrieve entity configuration YAML:  ",textStatus);
		});
	}

}

CCF.ndatransferconfig.initializeEntityConfigs = function() {

	if (typeof CCF.ndatransferconfig.entityConfigs === 'undefined') {
		$.ajax({
			type: "GET",
			url:serverRoot+'/xapi/ccfNdaTransferConfig/entityConfigs',
			cache: false,
			async: true,
			context: this,
			dataType: 'json'
		}).done( function(data, textStatus, jqXHR) {
			CCF.ndatransferconfig.entityConfigs = data;
		}).fail( function(data, textStatus, jqXHR) {
			console.log("ERROR:  Could not retrieve entity configuration YAML:  ",textStatus);
		});
	}

}

CCF.ndatransferconfig.initializePcpHelperInfo = function() {

	if (typeof CCF.ndatransferconfig.entityConfigs === 'undefined') {
		$.ajax({
			type: "GET",
			url:serverRoot+'/xapi/ccfNdaTransferConfig/getPcpHelperInfo',
			cache: false,
			async: true,
			context: this,
			dataType: 'json'
		}).done( function(data, textStatus, jqXHR) {
			CCF.ndatransferconfig.pcpHelpers = data;
		}).fail( function(data, textStatus, jqXHR) {
			console.log("ERROR:  Could not retrieve PCP Helper Info:  ",textStatus);
		});
	}

}

CCF.ndatransferconfig.initializeValueLists = function() {

	if (typeof CCF.ndatransferconfig.valueLists === 'undefined') {
		$.ajax({
			type: "GET",
			url:serverRoot+'/xapi/ccfNdaTransferConfig/valueLists',
			cache: false,
			async: true,
			context: this,
			dataType: 'json'
		}).done( function(data, textStatus, jqXHR) {
			CCF.ndatransferconfig.valueLists = data;
		}).fail( function(data, textStatus, jqXHR) {
			console.log("ERROR:  Could not retrieve value lists:  ",textStatus);
		});
	}

}

CCF.ndatransferconfig.createPackagesTable = function() {

	if (CCF.ndatransferconfig.currentData.length==0) {
		$(CCF.ndatransferconfig.currentTag).html("No transfer packages have been configured for this project.");
		return;
	}
	var tableHtml = "<table class='data-table xnat-table' style='width:100%;'>";
	tableHtml+="<tr><th></th><th class='center'>Package</th><th class='center'>Description</th><th></th></tr>";
	CCF.ndatransferconfig.currentData.sort(function(a,b) {
		return a.packageGroup - b.packageGroup;
	})
	for (var i=0; i<CCF.ndatransferconfig.currentData.length; i++) {
		var packageInfo = CCF.ndatransferconfig.currentData[i];
		CCF.ndatransferconfig.currentData[i]["__ID__"]=i.toString();
		tableHtml+="<tr>";
		tableHtml+="<td class='center'><a href='javascript:CCF.ndatransferconfig.packageInfoPanel(\"" + packageInfo.__ID__ + "\");'>View/Edit</a></td>";
		tableHtml+="<td>" + packageInfo.packageGroup + "</td>";
		tableHtml+="<td>" + packageInfo.packageDescription + "</td>";
		tableHtml+="<td class='center'><button onclick='CCF.ndatransferconfig.deletePackageConfig(\"" + packageInfo.__ID__ + "\")'>Delete</button></td>";
		tableHtml+="</tr>";
	}
	tableHtml+="</table>";
	$(CCF.ndatransferconfig.currentTag).html(tableHtml);

}

CCF.ndatransferconfig.getPackageConfig = function(__ID__) {

	for (var i=0; i<CCF.ndatransferconfig.currentData.length; i++) {
		var packageInfo = CCF.ndatransferconfig.currentData[i];
		if (__ID__.toString() === packageInfo.__ID__.toString()) {
			return packageInfo;
		}
	}
	return { fileSelectors: [] };

}

CCF.ndatransferconfig.getFileSelector = function(__ID__) {

	var fileSelectors = CCF.ndatransferconfig.updatePackageConfig.fileSelectors;
	for (var i=0; i<fileSelectors.length; i++) {
		var fileSelector =fileSelectors[i];
		if (__ID__.toString() === fileSelector.__ID__.toString()) {
			return fileSelector;
		}
	}
	return { fileMatchers: [] };

}

CCF.ndatransferconfig.getFileMatcher = function(__ID__) {

	var fileMatchers = CCF.ndatransferconfig.currentFileSelector.fileMatchers;
	for (var i=0; i<fileMatchers.length; i++) {
		var fileMatcher =fileMatchers[i];
		if (__ID__.toString() === fileMatcher.__ID__.toString()) {
			return fileMatcher;
		}
	}
	return {};

}

CCF.ndatransferconfig.getPackageDefinitionOptions = function() {

	var returnVar = {};
	var entityConfigs = CCF.ndatransferconfig.entityConfigs;
	var dataPackageDefinitions = entityConfigs.dataPackageDefinitionConfig;
	for (var clazz in dataPackageDefinitions) {
		if (dataPackageDefinitions.hasOwnProperty(clazz)) {
			var className = clazz.replace(/^.*[.]/,"");
			returnVar[clazz]=className;
		}
	}
	return returnVar;

}

CCF.ndatransferconfig.getFileSelectorOptions = function() {

	var returnVar = {};
	var entityConfigs = CCF.ndatransferconfig.entityConfigs;
	var fileSelectors = entityConfigs.fileSelectorConfig;
	for (var clazz in fileSelectors) {
		if (fileSelectors.hasOwnProperty(clazz)) {
			var className = clazz.replace(/^.*[.]/,"");
			returnVar[clazz]=className;
		}
	}
	return returnVar;

}

CCF.ndatransferconfig.getFileMatcherOptions = function() {

	var returnVar = {};
	var entityConfigs = CCF.ndatransferconfig.entityConfigs;
	var fileMatchers = entityConfigs.fileMatcherConfig;
	for (var clazz in fileMatchers) {
		if (fileMatchers.hasOwnProperty(clazz)) {
			var className = clazz.replace(/^.*[.]/,"");
			returnVar[clazz]=className;
		}
	}
	return returnVar;

}

CCF.ndatransferconfig.getPackageDefinitionYaml = function(canonicalClassname) {

	var entityConfigs = CCF.ndatransferconfig.entityConfigs;
	var dataPackageDefinitions = entityConfigs.dataPackageDefinitionConfig;
	for (var clazz in dataPackageDefinitions) {
		if (dataPackageDefinitions.hasOwnProperty(clazz)) {
			if (clazz == canonicalClassname) {
				var yamlArr = dataPackageDefinitions[clazz];
				var yaml = "";
				for (var i=0; i<yamlArr.length; i++) {
					yaml = yaml + yamlArr[i];
				}
				return yaml;
			}
		}
	}
	return "";

}

CCF.ndatransferconfig.getFileSelectorYaml = function(canonicalClassname) {

	var entityConfigs = CCF.ndatransferconfig.entityConfigs;
	var fileSelectors = entityConfigs.fileSelectorConfig;
	for (var clazz in fileSelectors) {
		if (fileSelectors.hasOwnProperty(clazz)) {
			if (clazz == canonicalClassname) {
				var yamlArr = fileSelectors[clazz];
				var yaml = "";
				for (var i=0; i<yamlArr.length; i++) {
					yaml = yaml + yamlArr[i];
				}
				return yaml;
			}
		}
	}
	return "";

}

CCF.ndatransferconfig.getFileMatcherYaml = function(canonicalClassname) {

	var entityConfigs = CCF.ndatransferconfig.entityConfigs;
	var fileMatchers = entityConfigs.fileMatcherConfig;
	for (var clazz in fileMatchers) {
		if (fileMatchers.hasOwnProperty(clazz)) {
			if (clazz == canonicalClassname) {
				var yamlArr = fileMatchers[clazz];
				var yaml = "";
				for (var i=0; i<yamlArr.length; i++) {
					yaml = yaml + yamlArr[i];
				}
				return yaml;
			}
		}
	}
	return "";

}

CCF.ndatransferconfig.packageInfoPanel = function(__ID__) {

	var isNew = (typeof __ID__ === 'undefined');
	var packageConfig = (isNew) ? { fileSelectors: [] } : CCF.ndatransferconfig.getPackageConfig(__ID__);
	CCF.ndatransferconfig.currentPackageConfig = packageConfig;
	CCF.ndatransferconfig.updatePackageConfig = JSON.parse(JSON.stringify(packageConfig));
	var packageDefinitionYaml = (isNew) ? "" : CCF.ndatransferconfig.getPackageDefinitionYaml(packageConfig.canonicalClassname);
	var packageDefinitionOptions = CCF.ndatransferconfig.getPackageDefinitionOptions();

	xmodal.open({
		title: ((isNew) ? ' New' : 'View/Edit') + ' Transfer Package Configuration',
		id: "view-modal",
		width: 850,
		height: 650,
		content: '<div id="viewPackageDiv"></div>',
		okLabel: 'Submit',
		okClose: false,
		cancel: 'Cancel',
		cancelAction: (function() {
			// Need to revert any values changed
			CCF.ndatransferconfig.createPackagesTable();
			xmodal.closeAll();
		}),
		okAction: (function() {
			setTimeout(function() {
				if (!CCF.ndatransferconfig.mousedClicked) {
					return;
				}
				// Need to revert any values changed
				CCF.ndatransferconfig.assignConfigValues();
				if (!CCF.ndatransferconfig.validateConfig()) {
					return;
				}
				if (CCF.ndatransferconfig.submitConfig()) {;
					xmodal.closeAll();
				}
			},50);
		})
	});





	/*
	var notifyOptions = {
		"ALWAYS": "Always",
		"ON_FAIL": "On Fail",
		"NONE": "Never"
	}
	*/

	var viewInfo = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				canonicalClassname: {
					kind: 'panel.select.single',
					id: 'canonicalClassname',
					name: 'canonicalClassname',
					className: 'required srconfig-ele',
					label: 'Package Definition Class:',
					value: packageConfig.canonicalClassname,
					options: packageDefinitionOptions
				}
			}
		}
	};

	CCF.ndatransferconfig.whenInserted("#canonicalClassname.srconfig-ele",function(ele) {
		$(".pcpconfig-ele").each(
			function(inx, e) {
				if (typeof $(e).attr('id') !== 'undefined') {
					CCF.ndatransferconfig.veChanger(e,false);
				}
			}
		);
		$(".pcpconfig-ele").keydown(
			function(e) {
				e.stopPropagation();
				CCF.ndatransferconfig.veChanger(e,true);
			}
		);
		$(".pcpconfig-ele").change(
			function(e) {
				e.stopPropagation();
				CCF.ndatransferconfig.veChanger(e,false);
			}
		);
	 });

	XNAT.spawner.spawn(viewInfo).render($("#viewPackageDiv")[0]);

	if (!isNew) {
		$("#view-modal").find('#canonicalClassname').prop('disabled',true);
	} else {
		$("#view-modal").find('#canonicalClassname').change(function() {
			var packageDefinitionYaml = CCF.ndatransferconfig.getPackageDefinitionYaml($("#view-modal").find('#canonicalClassname').val());
			var panel_body = $("#view-modal").find("#view-panel-panel").find(".panel-body")[0];
			CCF.ndatransferconfig.spawnIt(panel_body, packageDefinitionYaml);
			$("#view-modal").find('#canonicalClassname').prop('disabled',true);
			$(panel_body).find("#fileSelectors").html("<em>0" +
				" FileSelector(s) defined  <button onclick='CCF.ndatransferconfig.manageFileSelectors();' " +
				"style='margin-left:10px;padding:4px'>Manage FileSelectors</button>");
		});
	}

	var panel_body = $("#view-modal").find("#view-panel-panel").find(".panel-body")[0];
	CCF.ndatransferconfig.spawnIt(panel_body, packageDefinitionYaml);
	for (var key in packageConfig) {
		if (packageConfig.hasOwnProperty(key)) {
			var value = packageConfig[key];
			if (value && value.toString().length>0) {
				$(panel_body).find("#" + key).val(value);
			} else {
				$(panel_body).find("#" + key).val("");
			}
		}
	}
	CCF.ndatransferconfig.updatePackageConfig = packageConfig;
	//CCF.ndatransferconfig.populateFileSelectorsTable(packageConfig);
	CCF.ndatransferconfig.updateFileSelectors();
	/*
	var fileSelectors = (typeof packageConfig !== 'undefined' && typeof packageConfig.fileSelectors !== 'undefined') ? packageConfig.fileSelectors : [];
	$(panel_body).find("#fileSelectors").html("<em>" + fileSelectors.length + 
		" FileSelector(s) defined  <button onclick='CCF.ndatransferconfig.manageFileSelectors();' " +
		"style='margin-left:10px;padding:4px'>Manage FileSelectors</button>");
	*/

	var currentRecord = CCF.ndatransferconfig.getRecord(__ID__);
	if (typeof currentRecord === 'undefined' || typeof currentRecord.__ID__ === 'undefined') {
		return;
	}
	$("#input-dataType").val(currentRecord["dataType"]);

}

CCF.ndatransferconfig.assignConfigValues = function(packageConfig) {

	var updateConfig = CCF.ndatransferconfig.updatePackageConfig;
	if (typeof updateConfig.configuratbles === 'undefined') {
		updateConfig.configurables = {};
	}
	$("#view-panel-panel").find(".panel-body").find("[data-name]").each(function() {
		var dataName = $(this).attr("data-name");
		// NOTE:  We have special handling for arrays that may add an input element
		var configData = updateConfig[dataName];
		if (Array.isArray(configData)) {
			if (configData.length>0 && configData[0] === Object(configData[0])) {
				return true;
			}
		}
		var inputArr = $(this).find(":input");
		if (inputArr.length>0) {
			if ($(this).closest("div.configurableComponent").length==0) {
				updateConfig[dataName] = $(inputArr).val();
			} else {
				updateConfig.configurables[dataName] = $(inputArr).val();
			}
		}
	});

}

CCF.ndatransferconfig.veChanger = function(e, isKeyPress) {

	var eleVal = $(e).val();
	var eleId = $(e).attr('id');
	var divId = "configDiv-" + eleId;
	var divIdSel = "#" + divId;
	$(divIdSel).remove();
	var parDiv = $(e).closest("div.pcpconfig-ele");
	if (typeof CCF.ndatransferconfig.pcpHelpers !== 'undefined' && typeof CCF.ndatransferconfig.pcpHelpers.componentConfig !== 'undefined' &&
		Object.keys(CCF.ndatransferconfig.pcpHelpers.componentConfig).length>0) {
		var componentConfig = CCF.ndatransferconfig.pcpHelpers.componentConfig;
		var configurables = CCF.ndatransferconfig.currentPackageConfig.configurables;
		for (var key in componentConfig) {
			if (!componentConfig.hasOwnProperty(key) || key !== eleVal) {
				continue;
			}
			CCF.ndatransferconfig.whenInserted(divIdSel,function(e) {
				var spawnIt = componentConfig[key];
				for (var i=0; i<spawnIt.length; i++) {
					var parsedYaml = YAML.parse(spawnIt[i]);
					var func = this;
					CCF.ndatransferconfig.whenInserted(divIdSel + " :input", function() {
						if (typeof configurables === 'undefined') {
							return false;
						}
						$(divIdSel + " :input").each(function(inx, e) {
							var thisId = $(e).attr('id');
							if (typeof configurables !== 'undefined') {
								for (var vname in configurables) {
									if (!configurables.hasOwnProperty(vname) || vname !== thisId) {
										continue;
									}
									$(e).val(configurables[vname]);
									return true;
								}
							}
						});
					}.bind(this));
					CCF.ndatransferconfig.spawnComponentConfig(parsedYaml,divIdSel);
				}
			}.bind(this));
			$(parDiv).after("<div id='" + divId + "' class='configurableComponent'></div>");
			return;
		}
	}
}

CCF.ndatransferconfig.spawnComponentConfig = function(yaml, selector) {
	XNAT.spawner.spawn(yaml).render($(selector));
}

CCF.ndatransferconfig.validateConfig = function() {

	var updateConfig = CCF.ndatransferconfig.updatePackageConfig;
	var panel_body = $("#view-panel-panel").find(".panel-body");
	var isOk = true;
	var validationMsg = "ValidationError:  One or more validation checks failed<br><br><ul>";
	var currentData = CCF.ndatransferconfig.currentData;
	var packageGroupObj = $(panel_body).find("[data-name='packageGroup']").find(":input");
	var packageGroupVal = (typeof packageGroupObj !== 'undefined') ? $(packageGroupObj).val() : "";
	var packageDescriptionObj = $(panel_body).find("[data-name='packageDescription']").find(":input");
	var packageDescriptionVal = (typeof packageDescriptionObj !== 'undefined') ? $(packageDescriptionObj).val() : "";
	if ((packageGroupVal && packageGroupVal.toString().replace(/ /g,"").length>0) || 
	    (packageDescriptionVal && packageDescriptionVal.toString().replace(/ /g,"").length>0)) {
		for (var i=0; i<currentData.length; i++) {
			var currentRecord = currentData[i];
			if (currentRecord.__ID__ == updateConfig.__ID__) {
				continue;
			}
			if (currentRecord.packageGroup == packageGroupVal) {
				validationMsg = validationMsg + "<li>PackageGroup values must be unique (" + packageGroupVal + ")</li>";
				$(packageGroupObj).css("backgroundColor","#FFDDDD");
				isOk = false;
			} 
			if (currentRecord.packageDescription == packageDescriptionVal) {
				validationMsg = validationMsg + "<li>ImageCollectionDescription values must be unique (" + packageDescriptionVal + ")</li>";
				$(packageDescriptionObj).css("backgroundColor","#FFDDDD");
				isOk = false;
			} 
		}
	}
	if (!updateConfig.fileSelectors || updateConfig.fileSelectors.length<1) {
		validationMsg = validationMsg + "<li>No FileSelectors have been defined.  One or more file selectors are required for packages.</li>";
		$("#fileSelectors").css("backgroundColor","#FFDDDD");
		isOk = false;
	}
	var dataNameArr = $(panel_body).find("[data-name]");
	$(panel_body).find("[data-name]").each(function() {
		var returnObj = CCF.ndatransferconfig.validateField(this);
		validationMsg = validationMsg + returnObj.validationMsg;
		if (isOk) {
			isOk = returnObj.isOk;
		}
	});
	if (!isOk) {
		validationMsg = validationMsg + "<ul>";
		xmodal.message("Validation Failed", validationMsg, undefined, {height:"350px",width:"550px"});
	}
	return isOk;
	
}

CCF.ndatransferconfig.validateField = function(thisObj) {

	var dataObj = $(thisObj).find(":input");
	var returnObj = { validationMsg:"", isOk:true }
	if ($(dataObj).hasClass("required") || $(thisObj).hasClass("required")) {
		var dataName = $(thisObj).attr("data-name");
		dataName = (typeof dataName !== undefined) ? dataName : $(dataObj).attr("data-name");
		var value = $(dataObj).val();
		if (!value || value.toString().replace(/ /g,"").length<1) {
			returnObj.validationMsg = returnObj.validationMsg + "<li>Missing value for required field (" + dataName + ")</li>";
			$(dataObj).css("backgroundColor","#FFDDDD");
			returnObj.isOk = false;
		}
	} 
	if ($(dataObj).hasClass("integer") || $(thisObj).hasClass("integer")) {
		var dataName = $(thisObj).attr("data-name");
		dataName = (typeof dataName !== undefined) ? dataName : $(dataObj).attr("data-name");
		var value = $(dataObj).val();
		var trimValue = value.toString().replace(/ /g,"");
		if (value && !trimValue.length<1 && (trimValue !== "0" && !parseInt(value))) {
			returnObj.validationMsg = returnObj.validationMsg + "<li>Value for field (" + dataName + ") should be an integer</li>";
			$(dataObj).css("backgroundColor","#FFDDDD");
			returnObj.isOk = false;
		}
	} 
	return returnObj;

}


CCF.ndatransferconfig.validateFileSelector = function() {

	var isOk = true;
	var fileSelector = CCF.ndatransferconfig.currentFileSelector;
	var panel_body = $("#viewFileSelectorDiv").find("#view-panel-panel").find(".panel-body");
	var validationMsg = "ValidationError:  One or more validation checks failed<br><br><ul>";
	if (!fileSelector.fileMatchers || fileSelector.fileMatchers.length<1) {
		validationMsg = validationMsg + "<li>No FileMatchers have been defined.  One or more file matchers are required for selectors.</li>";
		$("#fileMatchers").css("backgroundColor","#FFDDDD");
		isOk = false;
	}
	$(panel_body).find("[data-name]").each(function() {
		var returnObj = CCF.ndatransferconfig.validateField(this);
		validationMsg = validationMsg + returnObj.validationMsg;
		if (isOk) {
			isOk = returnObj.isOk;
		}
	});
	if (!isOk) {
		validationMsg = validationMsg + "<ul>";
		xmodal.message("Validation Failed", validationMsg, undefined, {height:"350px",width:"550px"});
	}
	return isOk;
	
}


CCF.ndatransferconfig.validateFileMatcher = function() {

	var isOk = true;
	var fileMatcher = CCF.ndatransferconfig.currentFileMatcher;
	var panel_body = $("#viewFileMatcherDiv").find("#view-panel-panel").find(".panel-body");
	var validationMsg = "ValidationError:  One or more validation checks failed<br><br><ul>";
	$(panel_body).find("[data-name]").each(function() {
		var returnObj = CCF.ndatransferconfig.validateField(this);
		validationMsg = validationMsg + returnObj.validationMsg;
		if (isOk) {
			isOk = returnObj.isOk;
		}
	});
	if (!isOk) {
		validationMsg = validationMsg + "<ul>";
		xmodal.message("Validation Failed", validationMsg, undefined, {height:"350px",width:"550px"});
	}
	return isOk;

}

CCF.ndatransferconfig.submitConfig = function() {

	var updateConfig = CCF.ndatransferconfig.updatePackageConfig;
	var currentData = CCF.ndatransferconfig.currentData;
	if (typeof updateConfig !== 'undefined') {
		if (typeof updateConfig.__ID__ === 'undefined') {
			currentData.push(updateConfig);
		} else {
			currentData.configurables = updateConfig.configurables;
			for (var i=0; i<currentData.length; i++) {
				if (currentData[i].__ID__ == updateConfig.__ID__) {
					currentData[i] = updateConfig;
				}
			}
		}
	}
	CCF.pcpconfig.prevData = CCF.pcpconfig.currentData;
	$.ajax({
		type: "POST",
		url:serverRoot+'/xapi/ccfNdaTransferConfig/' + CCF.ndatransferconfig.currentProject + '/dataPackages',
		cache: false,
		async: true,
		context: this,
		data: JSON.stringify(CCF.ndatransferconfig.currentData),
		processData: false,
		contentType: 'application/json'
	}).done( function(data, textStatus, jqXHR) {
		XNAT.ui.banner.top(3000, 'Project package definitions have been updated', 'success');
		CCF.ndatransferconfig.createPackagesTable();
		CCF.pcpconfig.refreshPipelineCache('NdaTransferPipeline');
	}).fail( function(data, textStatus, jqXHR) {
		XNAT.ui.banner.top(3000, 'ERROR:  Project package definitions could not be updated', 'error');
		CCF.ndatransferconfig.createPackagesTable();
	});
	return true;
	
}

CCF.ndatransferconfig.spawnIt = function(panel_body, yaml) {
		
	if (yaml.length>0) {
		var parsedYaml = YAML.parse(yaml);
		XNAT.spawner.spawn(parsedYaml).render($(panel_body));
	}

}

CCF.ndatransferconfig.manageFileSelectors = function() {

	xmodal.open({
		title: 'Manage FileSelectors',
		id: "mfs-modal",
		width: 950,
		height: 475,
		content: '<div id="manageFileSelectorsDiv"></div>',
		okLabel: 'Done',
		okClose: true,
		cancel: 'Add New Selector',
		cancelLabel: 'Add New Selector',
		cancelClose: true,
		cancelAction: (function() {
			CCF.ndatransferconfig.viewEditFileSelector();
		}),
		okAction: (function() {
			CCF.ndatransferconfig.updateFileSelectors();
		})
	});
	CCF.ndatransferconfig.populateFileSelectorsTable();


}

CCF.ndatransferconfig.updateFileSelectors = function() {

	var fileSelectors = CCF.ndatransferconfig.updatePackageConfig.fileSelectors;
	$("#viewPackageDiv").find(".panel-body").find("#fileSelectors").html("<em>" + fileSelectors.length + 
		" FileSelector(s) defined  <button onclick='CCF.ndatransferconfig.manageFileSelectors();' " +
		"style='margin-left:10px;padding:4px'>Manage FileSelectors</button>");

}

CCF.ndatransferconfig.manageFileMatchers = function() {

	xmodal.open({
		title: 'Manage FileMatchers',
		id: "mfm-modal",
		width: 900,
		height: 425,
		content: '<div id="manageFileMatchersDiv"></div>',
		okLabel: 'Done',
		okClose: true,
		cancel: 'Add New Matcher',
		cancelLabel: 'Add New Matcher',
		cancelClose: true,
		cancelAction: (function() {
			CCF.ndatransferconfig.viewEditFileMatcher();
		}),
		okAction: (function() {
			CCF.ndatransferconfig.updateFileMatchers();
		})
	});
	CCF.ndatransferconfig.populateFileMatchersTable();


}

CCF.ndatransferconfig.viewEditFileSelector = function(__ID__) {

	var isNew = (typeof __ID__ === 'undefined');
	var fileSelector = (isNew) ? { fileMatchers: [] } : CCF.ndatransferconfig.getFileSelector(__ID__);
	var fileSelectorYaml = (isNew) ? "" : CCF.ndatransferconfig.getFileSelectorYaml(fileSelector.canonicalClassname);
	var fileSelectorOptions = CCF.ndatransferconfig.getFileSelectorOptions();

	xmodal.open({
		title: ((isNew) ? ' New' : 'View/Edit') + ' File Selector Configuration',
		id: "view-fs-modal",
		width: 800,
		height: 600,
		content: '<div id="viewFileSelectorDiv"></div>',
		okLabel: 'Done',
		okClose: false,
		cancel: 'Cancel',
		cancelClose: false,
		cancelAction: (function() {
			// Need to revert any values changed
			CCF.ndatransferconfig.populateFileSelectorsTable();
			xmodal.close("view-fs-modal");
		}),
		okAction: (function() {
			setTimeout(function() {
				if (!CCF.ndatransferconfig.mousedClicked) {
					return;
				}
				var fileSelector = CCF.ndatransferconfig.currentFileSelector;
				if (CCF.ndatransferconfig.validateFileSelector()) {
					$("#view-fs-modal").find("#view-panel-panel").find(".panel-body").find("[data-name]").each(function() {
						var dataName = $(this).attr("data-name");
						// NOTE:  We have special handling for arrays that may add an input element
						var configData = fileSelector[dataName];
						if (Array.isArray(configData)) {
							if (configData.length>0 && configData[0] === Object(configData[0])) {
								return;
							}
						}
						var inputArr = $(this).find(":input");
						if (inputArr.length>0) {
							fileSelector[dataName] = $(inputArr).val();
						}
					});
					if (typeof fileSelector.__ID__ === 'undefined') {
						var selectors = CCF.ndatransferconfig.updatePackageConfig.fileSelectors;
						var newId = 1;
						if (selectors.length>0) {
							for (var i=0; i<selectors.length; i++) {
								var selector = selectors[i];
								if (newId <= parseInt(selector.__ID__)) {
									newId = selector.__ID__ + 1;
								}
							}
						} 
						fileSelector.__ID__ = newId.toString();
						if (typeof CCF.ndatransferconfig.updatePackageConfig.fileSelectors === 'undefined' || 
							!Array.isArray(CCF.ndatransferconfig.updatePackageConfig.fileSelectors)) {
							CCF.ndatransferconfig.updatePackageConfig.fileSelectors = [];
						}
						CCF.ndatransferconfig.updatePackageConfig.fileSelectors.push(fileSelector);
					}
					CCF.ndatransferconfig.updateFileSelectors();
					CCF.ndatransferconfig.populateFileSelectorsTable();
					xmodal.close("view-fs-modal");
				}
			},50);
		})
	});

	/*
	var notifyOptions = {
		"ALWAYS": "Always",
		"ON_FAIL": "On Fail",
		"NONE": "Never"
	}
	*/

	var viewInfo = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				canonicalClassname: {
					kind: 'panel.select.single',
					id: 'canonicalClassname',
					name: 'canonicalClassname',
					className: 'required srconfig-ele',
					label: 'File Selector Definition Class:',
					value: fileSelector.canonicalClassname,
					options: fileSelectorOptions
				}
			}
		}
	};

	XNAT.spawner.spawn(viewInfo).render($("#viewFileSelectorDiv")[0]);

	if (!isNew) {
		$("#view-fs-modal").find('#canonicalClassname').prop('disabled',true);
	} else {
		$("#view-fs-modal").find('#canonicalClassname').change(function() {
			var fileSelectorYaml = CCF.ndatransferconfig.getFileSelectorYaml($("#view-fs-modal").find('#canonicalClassname').val());
			var panel_body = $("#view-fs-modal").find("#view-panel-panel").find(".panel-body")[0];
			CCF.ndatransferconfig.spawnIt(panel_body, fileSelectorYaml);
			$("#view-fs-modal").find('#canonicalClassname').prop('disabled',true);
			$(panel_body).find("#fileMatchers").html("<em>0" +
				" FileMatcher(s) defined  <button onclick='CCF.ndatransferconfig.manageFileMatchers();' " +
				"style='margin-left:10px;padding:4px'>Manage FileMatchers</button>");
		});
	}

	var panel_body = $("#view-fs-modal").find("#view-panel-panel").find(".panel-body")[0];
	CCF.ndatransferconfig.spawnIt(panel_body, fileSelectorYaml);
	for (var key in fileSelector) {
		if (fileSelector.hasOwnProperty(key)) {
			var value = fileSelector[key];
			if (value && value.toString().length>0) {
				$(panel_body).find("#" + key).val(value);
			} else {
				$(panel_body).find("#" + key).val("");
			}
		}
	}
	CCF.ndatransferconfig.currentFileSelector = fileSelector;
	//CCF.ndatransferconfig.populateFileSelectorsTable(packageConfig);
	var fileMatchers = (typeof fileSelector !== 'undefined' && typeof fileSelector.fileMatchers !== 'undefined') ? fileSelector.fileMatchers : [];
	$(panel_body).find("#fileMatchers").html("<em>" + fileMatchers.length + 
		" FileMatchers(s) defined  <button onclick='CCF.ndatransferconfig.manageFileMatchers();' " +
		"style='margin-left:10px;padding:4px'>Manage FileMatchers</button>");

	/*
	var currentRecord = CCF.ndatransferconfig.getRecord(__ID__);
	if (typeof currentRecord === 'undefined' || typeof currentRecord.__ID__ === 'undefined') {
		return;
	}
	$("#input-dataType").val(currentRecord["dataType"]);
	*/


}

CCF.ndatransferconfig.viewEditFileMatcher = function(__ID__) {

	var isNew = (typeof __ID__ === 'undefined');
	var fileMatcher = (isNew) ? {} : CCF.ndatransferconfig.getFileMatcher(__ID__);
	var fileMatcherYaml = (isNew) ? "" : CCF.ndatransferconfig.getFileMatcherYaml(fileMatcher.canonicalClassname);
	var fileMatcherOptions = CCF.ndatransferconfig.getFileMatcherOptions();

	xmodal.open({
		title: ((isNew) ? ' New' : 'View/Edit') + ' File Matcher Configuration',
		id: "view-fm-modal",
		width: 800,
		height: 600,
		content: '<div id="viewFileMatcherDiv"></div>',
		okLabel: 'Done',
		okClose: false,
		cancel: 'Cancel',
		cancelClose: false,
		cancelAction: (function() {
			// Need to revert any values changed
			CCF.ndatransferconfig.populateFileMatchersTable();
			xmodal.close("view-fm-modal");
		}),
		okAction: (function() {
			setTimeout(function() {
				if (!CCF.ndatransferconfig.mousedClicked) {
					return;
				}
				var fileMatcher = CCF.ndatransferconfig.currentFileMatcher;
				if (CCF.ndatransferconfig.validateFileMatcher()) {
					// Need to revert any values changed
					$("#view-fm-modal").find("#view-panel-panel").find(".panel-body").find("[data-name]").each(function() {
						var dataName = $(this).attr("data-name");
						// NOTE:  We have special handling for arrays that may add an input element
						var configData = fileMatcher[dataName];
						if (Array.isArray(configData)) {
							if (configData.length>0 && configData[0] === Object(configData)) {
								return;
							}
						}
						var inputArr = $(this).find(":input");
						if (inputArr.length>0) {
							fileMatcher[dataName] = $(inputArr).val();
						}
					});
					if (typeof fileMatcher.__ID__ === 'undefined') {
						var matchers = CCF.ndatransferconfig.currentFileSelector.fileMatchers;
						var newId = 1;
						if (matchers.length>0) {
							for (var i=0; i<matchers.length; i++) {
								var matcher = matchers[i];
								if (newId <= parseInt(matcher.__ID__)) {
									newId = matcher.__ID__ + 1;
								}
							}
						} 
						fileMatcher.__ID__ = newId.toString();
						if (typeof CCF.ndatransferconfig.currentFileSelector.fileMatchers === 'undefined' || 
							!Array.isArray(CCF.ndatransferconfig.currentFileSelector.fileMatchers)) {
							CCF.ndatransferconfig.currentFileSelector.fileMatchers = [];
						}
						CCF.ndatransferconfig.currentFileSelector.fileMatchers.push(fileMatcher);
					}
					CCF.ndatransferconfig.updateFileMatchers();
					CCF.ndatransferconfig.populateFileMatchersTable();
					xmodal.close("view-fm-modal");
				}
			},50);
		})
	});

	/*
	var notifyOptions = {
		"ALWAYS": "Always",
		"ON_FAIL": "On Fail",
		"NONE": "Never"
	}
	*/

	var viewInfo = {
		viewPanel: {
			kind: 'panel',
			header: false,
			footer: false,
			contents: {
				canonicalClassname: {
					kind: 'panel.select.single',
					id: 'canonicalClassname',
					name: 'canonicalClassname',
					className: 'required srconfig-ele',
					label: 'File Matcher Definition Class:',
					value: fileMatcher.canonicalClassname,
					options: fileMatcherOptions
				}
			}
		}
	};

	XNAT.spawner.spawn(viewInfo).render($("#viewFileMatcherDiv")[0]);

	if (!isNew) {
		$("#view-fm-modal").find('#canonicalClassname').prop('disabled',true);
	} else {
		$("#view-fm-modal").find('#canonicalClassname').change(function() {
			var fileMatcherYaml = CCF.ndatransferconfig.getFileMatcherYaml($("#view-fm-modal").find('#canonicalClassname').val());
			var panel_body = $("#view-fm-modal").find("#view-panel-panel").find(".panel-body")[0];
			CCF.ndatransferconfig.spawnIt(panel_body, fileMatcherYaml);
			$("#view-fm-modal").find('#canonicalClassname').prop('disabled',true);
			$(panel_body).find("#fileMatchers").html("<em>0" +
				" FileMatcher(s) defined  <button onclick='CCF.ndatransferconfig.manageFileMatchers();' " +
				"style='margin-left:10px;padding:4px'>Manage FileMatchers</button>");
		});
	}

	var panel_body = $("#view-fm-modal").find("#view-panel-panel").find(".panel-body")[0];
	CCF.ndatransferconfig.spawnIt(panel_body, fileMatcherYaml);
	for (var key in fileMatcher) {
		if (fileMatcher.hasOwnProperty(key)) {
			var value = fileMatcher[key];
			if (value && value.toString().length>0) {
				$(panel_body).find("#" + key).val(value);
			} else {
				$(panel_body).find("#" + key).val("");
			}
		}
	}
	CCF.ndatransferconfig.currentFileMatcher = fileMatcher;
	//CCF.ndatransferconfig.populateFileSelectorsTable(packageConfig);
	var fileMatchers = (typeof fileMatcher !== 'undefined' && typeof fileMatcher.fileMatchers !== 'undefined') ? fileMatcher.fileMatchers : [];

	/*
	var currentRecord = CCF.ndatransferconfig.getRecord(__ID__);
	if (typeof currentRecord === 'undefined' || typeof currentRecord.__ID__ === 'undefined') {
		return;
	}
	$("#input-dataType").val(currentRecord["dataType"]);
	*/

}

CCF.ndatransferconfig.updateFileMatchers = function() {

	var fileMatchers = CCF.ndatransferconfig.currentFileSelector.fileMatchers;
	$("#viewFileSelectorDiv").find(".panel-body").find("#fileMatchers").html("<em>" + fileMatchers.length + 
		" FileMatchers(s) defined  <button onclick='CCF.ndatransferconfig.manageFileMatchers();' " +
		"style='margin-left:10px;padding:4px'>Manage FileMatchers</button>");

}

CCF.ndatransferconfig.populateFileSelectorsTable = function() {

	var updateConfig = CCF.ndatransferconfig.updatePackageConfig;
	
	var fileSelectors = (typeof updateConfig !== 'undefined' && typeof updateConfig.fileSelectors !== 'undefined') ? updateConfig.fileSelectors : [];
	var prefOrder = [ "canonicalClassname","resourceType","matchRegex","excludeRegex","matchOperator","matchCount","fileMatchers","newLocation" ];
	var columns = [];
	for (var i=0; i<fileSelectors.length; i++) {
		var fileSelector = fileSelectors[i];
		fileSelectors[i]["__ID__"]=i.toString();
		for (var col in fileSelector) {
			if (!columns.includes(col)) {
				columns.push(col);
			}
		}
	}
	columns.sort(function(a,b)
		{
			var inx_a = prefOrder.indexOf(a);
			inx_a = (inx_a>=0) ? inx_a : 999;
			var inx_b = prefOrder.indexOf(b);
			inx_b = (inx_b>=0) ? inx_b : 999;
			return inx_a - inx_b;
		});

	var columnVar = CCF.ndatransferconfig.buildFsColumns(columns);

	$('#manageFileSelectorsDiv').html("")
	if (typeof fileSelectors !== 'undefined' && fileSelectors.length>0) {

		XNAT.table.dataTable(fileSelectors, {
		   table: {
		      id: 'fileSelectors-datatable',
		      className: 'fileSelectors-table sn-highlight'
		   },
		   width: '100%',
		   overflowY: 'hidden',
		   overflowX: 'auto',
		   columns: columnVar
		}).render($('#manageFileSelectorsDiv'));

	} else {

		$('#manageFileSelectorsDiv').html("No file selectors have been defined.");

	}
	

}

CCF.ndatransferconfig.populateFileMatchersTable = function() {

	var fileSelector = CCF.ndatransferconfig.currentFileSelector;
	
	var fileMatchers = (typeof fileSelector !== 'undefined' && typeof fileSelector.fileMatchers !== 'undefined') ? fileSelector.fileMatchers : [];
	var prefOrder = [ "canonicalClassname","resourceType","matchRegex","excludeRegex","matchOperator","matchCount","fileMatchers","newLocation" ];
	var columns = [];
	for (var i=0; i<fileMatchers.length; i++) {
		var fileMatcher = fileMatchers[i];
		fileMatchers[i]["__ID__"]=i.toString();
		for (var col in fileMatcher) {
			if (!columns.includes(col)) {
				columns.push(col);
			}
		}
	}
	columns.sort(function(a,b)
		{
			var inx_a = prefOrder.indexOf(a);
			inx_a = (inx_a>=0) ? inx_a : 999;
			var inx_b = prefOrder.indexOf(b);
			inx_b = (inx_b>=0) ? inx_b : 999;
			return inx_a - inx_b;
		});

	var columnVar = CCF.ndatransferconfig.buildFmColumns(columns);

	$('#manageFileMatchersDiv').html("")
	if (typeof fileMatchers !== 'undefined' && fileMatchers.length>0) {

		XNAT.table.dataTable(fileMatchers, {
		   table: {
		      id: 'fileMatchers-datatable',
		      className: 'fileMatchers-table sn-highlight'
		   },
		   width: '100%',
		   overflowY: 'hidden',
		   overflowX: 'auto',
		   columns: columnVar
		}).render($('#manageFileMatchersDiv'));

	} else {

		$('#manageFileMatchersDiv').html("No file matchers have been defined.");

	}
	

}

CCF.ndatransferconfig.buildFsColumns = function(columns) {
	var columnVar = {};
	columnVar["viewEditFs"]= {
	                label: " ",
	            	td: {'className': 'center'},
	                sort: false,
             		apply: function() {
				return "<td class='center'><a href='javascript:CCF.ndatransferconfig.viewEditFileSelector(" + this.__ID__ + ");'>View/Edit</a></td>";
			}
		};
	for (var i=0; i<columns.length; i++) {
		var col = columns[i];
		if (col === "__ID__") {
			continue;
		}
             	columnVar[col] = {
	                label: col,
	            	td: {'className': 'center'},
	                sort: false
            	};
		// Special field handling
		if (col == "canonicalClassname") {
             		columnVar[col]["label"] = "SelectorClass";
             		columnVar[col]["apply"] = function() {
				return this.canonicalClassname.toString().replace(/^.*[.]/,"");
			}
		} else if (col == "fileMatchers") {
             		columnVar[col]["apply"] = function() {
				return this.fileMatchers.length;
			}
		}
	}
	columnVar["deleteFs"]= {
	                label: " ",
	            	td: {'className': 'center'},
	                sort: false,
             		apply: function() {
				return "<button onclick='CCF.ndatransferconfig.deleteFileSelector(" + this.__ID__ + ")'>Delete</button>";
			}
		};
	return columnVar;
}

CCF.ndatransferconfig.buildFmColumns = function(columns) {
	var columnVar = {};
	columnVar["viewEditFm"]= {
	                label: " ",
	            	td: {'className': 'center'},
	                sort: false,
             		apply: function() {
				return "<td class='center'><a href='javascript:CCF.ndatransferconfig.viewEditFileMatcher(" + this.__ID__ + ");'>View/Edit</a></td>";
			}
		};
	for (var i=0; i<columns.length; i++) {
		var col = columns[i];
		if (col === "__ID__") {
			continue;
		}
             	columnVar[col] = {
	                label: col,
	            	td: {'className': 'center'},
	                sort: false
            	};
		// Special field handling
		if (col == "canonicalClassname") {
             		columnVar[col]["label"] = "MatcherClass";
             		columnVar[col]["apply"] = function() {
				return this.canonicalClassname.toString().replace(/^.*[.]/,"");
			}
		} else if (col == "fileMatchers") {
             		columnVar[col]["apply"] = function() {
				return this.fileMatchers.length;
			}
		}
	}
	columnVar["deleteFm"]= {
	                label: " ",
	            	td: {'className': 'center'},
	                sort: false,
             		apply: function() {
				return "<button onclick='CCF.ndatransferconfig.deleteFileMatcher(" + this.__ID__ + ")'>Delete</button>";
			}
		};
	return columnVar;
}

CCF.ndatransferconfig.deleteFileSelector = function(__ID__) {

	$(CCF.ndatransferconfig.updatePackageConfig.fileSelectors).each(function(inx) {
		if (typeof this.__ID__ !== 'undefined' && this.__ID__ == __ID__) {
			xmodal.confirm({
				title:  "Delete File Selector",
				content:  "<h3>Delete file selector:  " + this.matchRegex + "?",
				width: 500,
				height: 250,
				okLabel:  "OK",
				okClose:  true,
				okAction:  function() {
					CCF.ndatransferconfig.updatePackageConfig.fileSelectors.splice(inx,1);
					CCF.ndatransferconfig.updateFileSelectors();
					CCF.ndatransferconfig.populateFileSelectorsTable();
					xmodal.close("view-fs-modal");
				}
			});
			return false;
		}
	});

}

CCF.ndatransferconfig.deleteFileMatcher = function(__ID__) {

	$(CCF.ndatransferconfig.currentFileSelector.fileMatchers).each(function(inx) {
		if (typeof this.__ID__ !== 'undefined' && this.__ID__ == __ID__) {
			xmodal.confirm({
				title:  "Delete File Matcher",
				content:  "<h3>Delete file matcher:  " + this.matchRegex + "?",
				width: 500,
				height: 250,
				okLabel:  "OK",
				okClose:  true,
				okAction:  function() {
					CCF.ndatransferconfig.currentFileSelector.fileMatchers.splice(inx,1);
					CCF.ndatransferconfig.updateFileMatchers();
					CCF.ndatransferconfig.populateFileMatchersTable();
					xmodal.close("view-fm-modal");
				}
			});
			return false;
		}
	});

}

CCF.ndatransferconfig.deletePackageConfig = function(__ID__) {

	$(CCF.ndatransferconfig.currentData).each(function(inx) {
		if (typeof this.__ID__ !== 'undefined' && this.__ID__ == __ID__) {
			xmodal.confirm({
				title:  "Delete Package Definition",
				content:  "<h3>Delete package definition:  " + this.packageGroup + "?",
				width: 500,
				height: 250,
				okLabel:  "OK",
				okClose:  true,
				okAction:  function() {
					CCF.ndatransferconfig.currentData.splice(inx,1);
					if (CCF.ndatransferconfig.submitConfig()) {;
						xmodal.closeAll();
						CCF.ndatransferconfig.createPackagesTable();
					} else {
						xmodal.message("Error","Error saving data");
					}
				}
			});
			return false;
		}
	});

}

CCF.ndatransferconfig.getRecord = function(__ID__) {

	var currentRecord = {};
	$(CCF.ndatransferconfig.currentData).each(function(inx) {
		if (typeof this.__ID__ !== 'undefined' && this.__ID__ == __ID__) {
			currentRecord = this;
		}
	});
	return currentRecord;

}

CCF.ndatransferconfig.dataTypeOptions = function() {

	var options = {};
	var manifestTypeArr = CCF.ndatransferconfig.valueLists.NdaManifestType;
	$(manifestTypeArr).each(function(inx) {
		options[this.value] = this.displayName;
	});
	return options;

}

CCF.ndatransferconfig.populateSeriesDescriptionOptions = function() {
	var sdSelect = $("#viewPackageDiv").find("#seriesDescriptions,#facemaskingSeriesDescriptions");
	$.ajax({
		type: "GET",
		url:serverRoot+'/xapi/ccfNdaTransferConfig/' + CCF.ndatransferconfig.currentProject + '/getProjectSeriesDescriptions',
		cache: false,
		async: false,
		context: this,
		dataType: 'json'
	}).done( function(data, textStatus, jqXHR) {
		$(data).each(function(inx,e) {
			$(sdSelect).append("<option value='" + e + "'>" + e + "</option>");
		});
		$(sdSelect).val("");
	});
}

$(document).ready(function() {
	$(document).keypress(
		function(){
	    		if (event.which == '13') {
				CCF.ndatransferconfig.enterPressed = true;
				CCF.ndatransferconfig.mousedClicked = false;
	    		}
		}
	);
	$(document).mousedown(
		function(){
			CCF.ndatransferconfig.enterPressed = false;
			CCF.ndatransferconfig.mousedClicked = true;
		}
	);
});

