package org.nrg.ccf.ndatransfer.client.entities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class ManifestEntry {

	public ManifestEntry(String path, File target, boolean computeMd5) {
		this.path = path;
		name = (path.contains("/")) ? path.substring(path.lastIndexOf('/')+1) : path;
		if (!target.exists()) {
			log.warn("ERROR:  Requested manifest file does not exist:  {}", target.getPath());
			return;
		}
		size = String.valueOf(target.length());
		md5sum = "";
		try {
			if (computeMd5) {
				md5sum = DigestUtils.md5Hex(FileUtils.openInputStream(target));
			}
		} catch (IOException e1) {
			log.warn("ERROR:  Couldn't compute md5sum for manifest file:  {}", target.getPath());
		}
	}
	
	private String path;
	private String name;
	private String size;
	private String md5sum;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((md5sum == null) ? 0 : md5sum.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ManifestEntry other = (ManifestEntry) obj;
		if (md5sum == null) {
			if (other.md5sum != null)
				return false;
		} else if (!md5sum.equals(other.md5sum))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (size == null) {
			if (other.size != null)
				return false;
		} else if (!size.equals(other.size))
			return false;
		return true;
	}
	

}
