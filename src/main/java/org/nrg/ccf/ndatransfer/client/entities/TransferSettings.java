package org.nrg.ccf.ndatransfer.client.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransferSettings {

	private String ndaApiUrl;
	private String pathToVtcmd;
	private Integer defaultCollectionId;
	private Integer timeoutMinutes;
	private String tempdirLocation;

}
