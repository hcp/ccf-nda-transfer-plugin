package org.nrg.ccf.ndatransfer.client;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.apache.commons.configuration2.INIConfiguration;
import org.apache.commons.configuration2.SubnodeConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jaxb.internal.XmlCollectionJaxbProvider;
import org.glassfish.jersey.jaxb.internal.XmlJaxbElementProvider;
import org.glassfish.jersey.jaxb.internal.XmlRootElementJaxbProvider;
import org.glassfish.jersey.jaxb.internal.XmlRootObjectJaxbProvider;
import org.nrg.ccf.ndatransfer.client.entities.ProjectTransferSettings;
import org.nrg.ccf.ndatransfer.client.entities.SubmissionResource;
import org.nrg.ccf.ndatransfer.client.entities.SubmissionResourceItem;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionEntity;
import org.nrg.ccf.ndatransfer.exception.NdaRestClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zalando.jersey.gson.GsonProvider;
//import com.owlike.genson.ext.jsr353.GensonJsonProvider;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SubmissionServiceClient {
	
	private Client _client;
	private ProjectTransferSettings _projectTransferSettings;
	private final Map<Long,List<SubmissionResourceItem>> _cachedResources = new ConcurrentHashMap<>();
	private final Map<Long,Date> _cacheTimes = new ConcurrentHashMap<>();
	private final int CACHE_EXPIRE_MINUTES = 5;
	private final String SUBMISSION_RESOURCES_URL_PORTION = "/submission";
	
	@Autowired
	public SubmissionServiceClient(ProjectTransferSettings projectTransferSettings) {
		super();
		_projectTransferSettings = projectTransferSettings;
	}

	@PostConstruct
	private void initIt() {
		try {
			_client = ClientBuilder.newClient();
			final File settingsFl = new File(System.getProperty("user.home") + File.separator + ".NDATools","settings.cfg"); 
			if (!(settingsFl.exists() && settingsFl.canRead())) {
				throw new NdaRestClientException("ERROR:  The NDATools settings does not exist or is not readable");
			}
			final INIConfiguration config = new Configurations().ini(settingsFl);
			SubnodeConfiguration section = config.getSection("User");
			if (section == null || section.isEmpty()) {
				throw new NdaRestClientException("ERROR:  The NDATools settings file user section is empty or null");
			}
			final String username = section.getString("username");
			final String password = section.getString("password");
			HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(username, password.getBytes());
			_client.register(feature);
		} catch (Exception e) {
			log.error("Initialization Exception: ",e);
		}
	}
	
	@SuppressWarnings("unused")
	public SubmissionResource getSubmissionResource(final String projectId, String submissionId) {
		Client client;
		try {
			client = ClientBuilder.newClient();
			client.register(GsonProvider.class);
			//client.register(GensonJsonProvider.class);
			client.register(XmlCollectionJaxbProvider.class);
			client.register(XmlJaxbElementProvider.class);
			client.register(XmlRootElementJaxbProvider.class);
			client.register(XmlRootObjectJaxbProvider.class);
			final File settingsFl = new File(System.getProperty("user.home") + File.separator + ".NDATools","settings.cfg"); 
			if (!(settingsFl.exists() && settingsFl.canRead())) {
				throw new NdaRestClientException("ERROR:  The NDATools settings does not exist or is not readable");
			}
			final INIConfiguration config = new Configurations().ini(settingsFl);
			SubnodeConfiguration section = config.getSection("User");
			if (section == null || section.isEmpty()) {
				throw new NdaRestClientException("ERROR:  The NDATools settings file user section is empty or null");
			}
			final String username = section.getString("username");
			final String password = section.getString("password");
			HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(username, password.getBytes());
			client.register(feature);
		} catch (Exception e) {
			log.error("Initialization Exception: ",e);
			return null;
		}
		final String ndaApiUrl = _projectTransferSettings.getNdaApiUrl(projectId);
		final String submissionResourcesUrl = ndaApiUrl + SUBMISSION_RESOURCES_URL_PORTION + "/" + submissionId;
		WebTarget target = client.target(submissionResourcesUrl);
		return target.request(MediaType.APPLICATION_XML).get(new GenericType<SubmissionResource>(){});
		
	}
	
	public SubmissionResource getSubmissionResourceForSubmissionEntity(NdaSubmissionEntity submissionEntity, final boolean useCachedValue) {
		for (final SubmissionResource  resource : getSubmissionResources(submissionEntity.getProjectId(), Long.valueOf(submissionEntity.getCollectionId()), useCachedValue)) {
			if (resource.getDataset_title().equals(submissionEntity.getSubmissionDatasetTitle())) {
				return resource;
			}
		}
		return null;
	}
	
	public List<SubmissionResource> getSubmissionResources(final String projectId, final Long collectionId, final boolean useCachedValue) {
		final Date currentTime = new Date();
		if (useCachedValue && _cacheTimes.containsKey(collectionId)) {
			if (currentTime.getTime()-_cacheTimes.get(collectionId).getTime()<(1000*60*CACHE_EXPIRE_MINUTES)) {
				final List<SubmissionResource> returnList = new ArrayList<>();
				returnList.addAll(_cachedResources.get(collectionId));
				return returnList;
			}
		} 
		return getSubmissionResources(projectId, collectionId, null, false);
	}
	
	public List<SubmissionResource> getSubmissionResources(String projectId, Long collectionId, final String status, final boolean usersOwnSubmissions) {
		return getSubmissionResources(projectId, new Long[] { collectionId }, status, usersOwnSubmissions);
	}
	
	public List<SubmissionResource> getSubmissionResources(final String projectId, final Long[] collectionId, final String status, final boolean usersOwnSubmissions) {
		Client client;
		try {
			client = ClientBuilder.newClient();
			client.register(GsonProvider.class);
			//client.register(GensonJsonProvider.class);
			client.register(XmlCollectionJaxbProvider.class);
			client.register(XmlJaxbElementProvider.class);
			client.register(XmlRootElementJaxbProvider.class);
			client.register(XmlRootObjectJaxbProvider.class);
			final File settingsFl = new File(System.getProperty("user.home") + File.separator + ".NDATools","settings.cfg"); 
			if (!(settingsFl.exists() && settingsFl.canRead())) {
				throw new NdaRestClientException("ERROR:  The NDATools settings does not exist or is not readable");
			}
			final INIConfiguration config = new Configurations().ini(settingsFl);
			SubnodeConfiguration section = config.getSection("User");
			if (section == null || section.isEmpty()) {
				throw new NdaRestClientException("ERROR:  The NDATools settings file user section is empty or null");
			}
			final String username = section.getString("username");
			final String password = section.getString("password");
			if (username == null || password == null || username.isEmpty() || password.isEmpty()) {
				throw new NdaRestClientException("ERROR:  NDATools Username or password is null - " + 
						"NOTE:  Tool now requires keyrings so this client does not currently work.");
			}
			HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(username, password.getBytes());
			client.register(feature);
		} catch (Exception e) {
			log.error("ERROR: Initialization exception:" + e.toString());
			log.error("NOTE: This client no longer functions.  Password authentication is no longer supported.  Might" +
			   "might need to update to keyring authentication and there may have been other API changes");
			return null;
		}
		final String ndaApiUrl = _projectTransferSettings.getNdaApiUrl(projectId);
		// NOTE:  2019/09/23.  Began returning a 301 if the trailing "/" wasn't supplied.  It's now required here.
		final String submissionResourcesUrl = ndaApiUrl + SUBMISSION_RESOURCES_URL_PORTION + "/";
		WebTarget target = client.target(submissionResourcesUrl);
		if (status != null && !status.isEmpty()) {
			target.queryParam("status", status);
		}
		if (collectionId != null && !(collectionId.length==0)) {
			for (final Long id : collectionId) {
				target.queryParam("collectionId", id);
			}
		}
		target.queryParam("usersOwnSubmissions", usersOwnSubmissions);
		List<SubmissionResourceItem> returnedResources = target.request(MediaType.APPLICATION_XML).get(new GenericType<List<SubmissionResourceItem>>(){});
		if (status == null && !usersOwnSubmissions) {
			_cacheTimes.put(collectionId[0], new Date());
			_cachedResources.put(collectionId[0], returnedResources);
			clearOutOldCachedValues();
		}
		List<SubmissionResource> submissionResources = new ArrayList<>();
		submissionResources.addAll(returnedResources);
		return submissionResources;
		
	}

	private void clearOutOldCachedValues() {
		final Date currentTime =  new Date();
		for (Long collectionId : _cacheTimes.keySet()) {
			if (currentTime.getTime()-_cacheTimes.get(collectionId).getTime()>=(1000*60*CACHE_EXPIRE_MINUTES)) {
				_cachedResources.remove(collectionId);
				_cacheTimes.remove(collectionId);
			}
		}
	}

	public void clearCollectionCache(final Long collectionId) {
		if (_cacheTimes.containsKey(collectionId)) {
			_cachedResources.remove(collectionId);
			_cacheTimes.remove(collectionId);
		}
	}
	
}
