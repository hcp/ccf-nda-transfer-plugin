package org.nrg.ccf.ndatransfer.client.entities;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="links")
public class Links {
	
	//@XmlElement
	private String rel;
	//@XmlElement
	private String href;
	
	@XmlElement
	public String getRel() {
		return rel;
	}
	
	public void setRel(String rel) {
		this.rel = rel;
	}
	
	@XmlElement
	public String getHref() {
		return href;
	}
	
	public void setHref(String href) {
		this.href = href;
	}

}
