package org.nrg.ccf.ndatransfer.client.entities;

import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.beans.AbstractPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.services.NrgPreferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@NrgPreferenceBean(toolId = "ndaTransferSiteSettings", toolName = "NDA Transfer Site Settings")
public class SiteTransferSettings extends AbstractPreferenceBean {

	private static final long serialVersionUID = 7698152330535034996L;

	@Autowired
	protected SiteTransferSettings(NrgPreferenceService preferenceService) {
		super(preferenceService);
	}

	@NrgPreference
	public String getNdaApiUrl() {
		return this.getValue("ndaApiUrl");
	}

	public void setNdaApiUrl(final String ndaApiUrl) {
		try {
			this.set(ndaApiUrl, "ndaApiUrl");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getPathToVtcmd() {
		return this.getValue("pathToVtcmd");
	}

	public void setPathToVtcmd(final String pathToVtcmd) {
		try {
			this.set(pathToVtcmd, "pathToVtcmd");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	public TransferSettings getTransferSettings() {
		final TransferSettings settings = new TransferSettings();
		settings.setNdaApiUrl(this.getNdaApiUrl());
		settings.setPathToVtcmd(this.getPathToVtcmd());
		settings.setDefaultCollectionId(null);
		return settings;
	}

}
