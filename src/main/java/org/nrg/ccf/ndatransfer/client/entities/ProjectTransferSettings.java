package org.nrg.ccf.ndatransfer.client.entities;

import org.nrg.ccf.common.utilities.abst.AbstractProjectPreferenceBean;
import org.nrg.ccf.common.utilities.components.PreferenceUtils;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.services.NrgPreferenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@NrgPreferenceBean(toolId = "ndaTransferProjectSettings", toolName = "NDA Transfer Project Settings")
public class ProjectTransferSettings extends AbstractProjectPreferenceBean {

	private static final long serialVersionUID = -1938294777802898215L;

	@Autowired
	protected ProjectTransferSettings(NrgPreferenceService preferenceService, PreferenceUtils preferenceUtils) {
		super(preferenceService, preferenceUtils);
	}
	
	@NrgPreference
	public String getNdaApiUrl() {
		return "";
	}

	public String getNdaApiUrl(final String entityId) {
		return this.getValue(SCOPE, entityId, "ndaApiUrl");
	}

	public void setNdaApiUrl(final String entityId, final String ndaApiUrl) {
		try {
			this.set(SCOPE, entityId, ndaApiUrl, "ndaApiUrl");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getPathToVtcmd() {
		return "";
	}
	
	public String getPathToVtcmd(final String entityId) {
		return this.getValue(SCOPE, entityId, "pathToVtcmd");
	}

	public void setPathToVtcmd(final String entityId, final String pathToVtcmd) {
		try {
			this.set(SCOPE, entityId, pathToVtcmd, "pathToVtcmd");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public Integer getDefaultCollectionId() {
		return null;
	}
	
	public Integer getDefaultCollectionId(final String entityId) {
		try {
			return this.getIntegerValue(SCOPE, entityId, "defaultCollectionId");
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public void setDefaultCollectionId(final String entityId, final Integer defaultCollectionId) {
		try {
			this.setIntegerValue(SCOPE, entityId, defaultCollectionId, "defaultCollectionId");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}


	@NrgPreference
	public Integer getTimeoutMinutes() {
		return null;
	}
	
	public Integer getTimeoutMinutes(final String entityId) {
		try {
			return this.getIntegerValue(SCOPE, entityId, "timeoutMinutes");
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public void setTimeoutMinutes(final String entityId, final Integer timeoutMinutes) {
		try {
			this.setIntegerValue(SCOPE, entityId, timeoutMinutes, "timeoutMinutes");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	@NrgPreference
	public String getTempdirLocation() {
		return null;
	}
	
	public String getTempdirLocation(final String entityId) {
		try {
			return this.getValue(SCOPE, entityId, "tempdirLocation");
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public void setTempdirLocation(final String entityId, final String tempdirLocation) {
		try {
			this.set(SCOPE, entityId, tempdirLocation, "tempdirLocation");
		} catch (InvalidPreferenceName invalidPreferenceName) {
			log.error("Invalid preference Name", invalidPreferenceName);
		}
	}

	public TransferSettings getTransferSettings(String projectId) {
		final TransferSettings settings = new TransferSettings();
		settings.setNdaApiUrl(this.getNdaApiUrl(projectId));
		settings.setPathToVtcmd(this.getPathToVtcmd(projectId));
		settings.setDefaultCollectionId(this.getDefaultCollectionId(projectId));
		settings.setTimeoutMinutes(this.getTimeoutMinutes(projectId));
		settings.setTempdirLocation(this.getTempdirLocation(projectId));
		return settings;
	}

}
