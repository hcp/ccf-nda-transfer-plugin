package org.nrg.ccf.ndatransfer.client.entities;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public class ManifestFileContents {

	private final List<ManifestEntry> files = new ArrayList<>();
	
	public ManifestFileContents(List<ManifestEntry> files) {
		super();
		this.files.addAll(files);
	}

}
