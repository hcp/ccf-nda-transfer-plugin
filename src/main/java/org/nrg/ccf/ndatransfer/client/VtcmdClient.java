package org.nrg.ccf.ndatransfer.client;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.common.utilities.utils.ScriptExecUtils;
import org.nrg.ccf.ndatransfer.client.entities.ProjectTransferSettings;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionEntity;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionStatement;
import org.nrg.ccf.common.utilities.components.ScriptResult;
import org.nrg.ccf.ndatransfer.entities.UploadBundle;
import org.nrg.ccf.ndatransfer.exception.ResumeSubmissionException;
import org.nrg.ccf.ndatransfer.exception.ValidateAndUploadException;
import org.nrg.ccf.ndatransfer.services.NdaSubmissionStatementService;
import org.nrg.ccf.ndatransfer.utils.MiscUtils;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class VtcmdClient {
	
	private ProjectTransferSettings _projectTransferSettings;
	private NdaSubmissionStatementService _statementService;
	
	@Autowired
	public VtcmdClient(final ProjectTransferSettings projectTransferSettings, final NdaSubmissionStatementService statementService) {
		super();
		_projectTransferSettings = projectTransferSettings;
		_statementService = statementService;
	}
	
	public ScriptResult validate(final UserI user, final UploadBundle bundle, final File tempDir, final String additionalParameters) throws ValidateAndUploadException {
		
		return doSubmission(user, bundle, tempDir, additionalParameters, true);
		
	}
	
	public ScriptResult validateAndUpload(final UserI user, final UploadBundle bundle, final File tempDir, final String additionalParameters) throws ValidateAndUploadException {
		
		return doSubmission(user, bundle, tempDir, additionalParameters, false);
		
	}
	
	public ScriptResult resumeSubmission(final UserI user, final NdaSubmissionEntity submissionEntity, String additionalParameters, boolean resumeWithoutSubmissionId) throws ResumeSubmissionException {
		final String[] cmdSegs = getResumeSubmitCommand(user, submissionEntity, additionalParameters, resumeWithoutSubmissionId);
		if (cmdSegs.length>1) {
			return ScriptExecUtils.execRuntimeCommand(cmdSegs, _projectTransferSettings.getTimeoutMinutes(submissionEntity.getProjectId()), log, true);
		} else {
			return ScriptExecUtils.execRuntimeCommand(cmdSegs[0], _projectTransferSettings.getTimeoutMinutes(submissionEntity.getProjectId()), log, true);
		}
	}
	
	public String getResumeSubmitCommandStr(final UserI user, final NdaSubmissionEntity submissionEntity, String additionalParameters, boolean resumeWithoutSubmissionId) throws ResumeSubmissionException {
		return MiscUtils.arrayToString(getResumeSubmitCommand(user, submissionEntity, additionalParameters, resumeWithoutSubmissionId));
	}
	
	private String[] getResumeSubmitCommand(final UserI user, final NdaSubmissionEntity submissionEntity, String additionalParameters, boolean resumeWithoutSubmissionId) throws ResumeSubmissionException {
		final List<String> cmdSegs = new ArrayList<>();
		cmdSegs.add(_projectTransferSettings.getPathToVtcmd(submissionEntity.getProjectId()));
		final String tempDirStr = submissionEntity.getTempDirectory();
		final String dataFileDirStr = (tempDirStr != null) ? new StringBuilder(tempDirStr).append("/files").toString() : null;
		final File dataFileDir = (dataFileDirStr != null && dataFileDirStr.length()>0) ? new File(dataFileDirStr) : null;
		if (dataFileDir == null || !dataFileDir.isDirectory()) {
			throw new ResumeSubmissionException("ERROR: Data file directory is null or no longer exists");
		}
		if (!resumeWithoutSubmissionId) {
			final Integer submissionId = submissionEntity.getSubmissionId();
			if (submissionId == null) {
				throw new ResumeSubmissionException("ERROR: SubmissionID is null");
			}
			cmdSegs.add("--resume");
			cmdSegs.add(submissionId.toString());
			cmdSegs.add("-l");
			cmdSegs.add(dataFileDirStr);
			cmdSegs.add("--hideProgress");
			for (final String addParam : additionalParameters.split(" ")) {
				cmdSegs.add(addParam);
			}
			log.debug("Constructing command-line command from the following segments:");
			log.debug(cmdSegs.toString());
			return cmdSegs.toArray(new String[] {});
		} else {
			final NdaSubmissionStatement statement = _statementService.findByTempDir(tempDirStr);
			if (statement == null) {
				throw new ResumeSubmissionException("ERROR: Couldn't retreive submitted statement for temp directory");
			}
			String submitStr = statement.getSubmissionStatement();
			if (additionalParameters != null && additionalParameters.trim().length()>0 && !statement.getSubmissionStatement().contains(additionalParameters)) {
				submitStr = submitStr + " " + additionalParameters;
			}
			return new String[] {submitStr};
		}
	}
	
	public String getSubmissionCommandStr(final UserI user, final UploadBundle bundle, File tempDir, final String additionalParameters, boolean validateOnly) throws ValidateAndUploadException {
		return MiscUtils.arrayToString(getSubmissionCommand(user, bundle, tempDir, additionalParameters, validateOnly));
	}
		
	private String[] getSubmissionCommand(final UserI user, final UploadBundle bundle, File tempDir, final String additionalParameters, boolean validateOnly) throws ValidateAndUploadException {
		final List<String> cmdSegs = new ArrayList<>();
		cmdSegs.add(_projectTransferSettings.getPathToVtcmd(bundle.getProjectId()));
		final String exportCsv = bundle.getExportCsv();
		final List<String> manifestDir = bundle.getManifestDirs();
		final List<String> datafileDir = bundle.getDatafileDirs();
		final String submissionDesc = bundle.getSubmissionDesc();
		final String submissionTitle = bundle.getSubmissionTitle();
		final Integer collectionId = bundle.getCollectionId();
		if (exportCsv == null || exportCsv.isEmpty()) {
			throw new ValidateAndUploadException("ERROR: You must supply an export file");
		}
		final File exportCsvF = new File(exportCsv);
		if (!(exportCsvF.exists() && exportCsvF.isFile() && exportCsvF.canRead())) {
			throw new ValidateAndUploadException("ERROR: The export file specified either does not exist, is not a file, or is not accessible"); 
		}
		cmdSegs.add(exportCsv);
		if (!manifestDir.isEmpty()) {
			cmdSegs.add("-m");
			cmdSegs.add(MiscUtils.listToString(manifestDir));
		}
		if (datafileDir!=null && !datafileDir.isEmpty()) {
			cmdSegs.add("-l");
			cmdSegs.add(MiscUtils.listToString(datafileDir));
		}
		if (submissionTitle == null || submissionTitle.isEmpty()) {
			throw new ValidateAndUploadException("ERROR: You must provide a submission title"); 
		}
		cmdSegs.add("-t");
		cmdSegs.add(submissionTitle);
		if (submissionDesc != null && !submissionDesc.isEmpty()) {
			cmdSegs.add("-d");
			cmdSegs.add("'" + submissionDesc.replaceAll("'", "\\'") + "'");
		}
		if (collectionId == null || collectionId<=0) {
			throw new ValidateAndUploadException("ERROR: The collection ID specified is missing or invalid"); 
		} else {
			cmdSegs.add("-c");
			cmdSegs.add(collectionId.toString());
		}
		if (!validateOnly) {
			cmdSegs.add("-b");
		}
		cmdSegs.add("--hideProgress");
		for (final String addParam : additionalParameters.split(" ")) {
			if (addParam != null && addParam.trim().length()>0) {
				cmdSegs.add(addParam);
			}
		}
		log.debug("Constructing command-line command from the following segments:");
		log.debug(cmdSegs.toString());
		final String[] cmdArray = cmdSegs.toArray(new String[] {});
		return cmdArray;
	}
	
		
	private ScriptResult doSubmission(final UserI user, final UploadBundle bundle, File tempDir, final String additionalParameters, boolean validateOnly) throws ValidateAndUploadException {
		final String[] cmdArray = getSubmissionCommand(user, bundle, tempDir, additionalParameters, validateOnly);
		try {
			NdaSubmissionStatement statementEntity = _statementService.findByTempDir(tempDir.getCanonicalPath());
			if (statementEntity == null) {
				statementEntity = new NdaSubmissionStatement(tempDir.getCanonicalPath(), MiscUtils.arrayToString(cmdArray));
				_statementService.create(statementEntity);
			} else {
				statementEntity.setSubmissionStatement(MiscUtils.arrayToString(cmdArray));
				_statementService.update(statementEntity);
			}
		} catch (IOException e) {
			log.error("Exception thrown saving submission statement:", e);
		}
		return ScriptExecUtils.execRuntimeCommand(cmdArray, _projectTransferSettings.getTimeoutMinutes(bundle.getProjectId()), log, true);
	}
	
}
