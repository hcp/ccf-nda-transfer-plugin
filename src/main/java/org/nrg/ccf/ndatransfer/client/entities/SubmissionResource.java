package org.nrg.ccf.ndatransfer.client.entities;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="Submission_Resource")
public class SubmissionResource {
	
	//@XmlElement
	private List<Links> links;
	//@XmlElement
	private String submission_id;
	//@XmlElement
	private String submission_status;
	//@XmlElement
	private String dataset_title;
	//@XmlElement
	private String dataset_description;
	//@XmlElement
	private Date dataset_created_date;
	//@XmlElement
	private Date dataset_modified_date;
	//@XmlElement
	private NdaCollection collection;
	
	@XmlElement
	public List<Links> getLinks() {
		return links;
	}
	
	public void setLinks(List<Links> links) {
		this.links = links;
	}
	
	@XmlElement
	public String getSubmission_id() {
		return submission_id;
	}
	
	public void setSubmission_id(String submission_id) {
		this.submission_id = submission_id;
	}
	
	@XmlElement
	public String getSubmission_status() {
		return submission_status;
	}
	
	public void setSubmission_status(String submission_status) {
		this.submission_status = submission_status;
	}
	
	@XmlElement
	public String getDataset_title() {
		return dataset_title;
	}
	
	public void setDataset_title(String dataset_title) {
		this.dataset_title = dataset_title;
	}
	
	@XmlElement
	public String getDataset_description() {
		return dataset_description;
	}
	
	public void setDataset_description(String dataset_description) {
		this.dataset_description = dataset_description;
	}
	
	@XmlElement
	public Date getDataset_created_date() {
		return dataset_created_date;
	}
	
	public void setDataset_created_date(Date dataset_created_date) {
		this.dataset_created_date = dataset_created_date;
	}
	
	@XmlElement
	public Date getDataset_modified_date() {
		return dataset_modified_date;
	}
	
	public void setDataset_modified_date(Date dataset_modified_date) {
		this.dataset_modified_date = dataset_modified_date;
	}
	
	@XmlElement
	public NdaCollection getCollection() {
		return collection;
	}
	
	public void setCollection(NdaCollection collection) {
		this.collection = collection;
	}

}
