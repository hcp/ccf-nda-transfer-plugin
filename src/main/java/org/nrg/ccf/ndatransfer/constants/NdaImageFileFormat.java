package org.nrg.ccf.ndatransfer.constants;

import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum NdaImageFileFormat implements ValueInfoI {
	
	AFNI("AFNI"), ANALYZE("ANALYZE"), AVI("AVI"), BIORAD("BIORAD"), BMP("BMP"), BRIK("BRIK"), BRUKER("BRUKER"),
	CHESHIRE("CHESHIRE"), COR("COR"), DICOM("DICOM"), DM3("DM3"), FITS("FITS"), GE_GENESIS("GE GENESIS"),
	GE_SIGNA4X("GE SIGNA4X"), GIF("GIF"), HEAD("HEAD"), ICO("ICO"), ICS("ICS"), INTERFILE("INTERFILE"),
	JPEG("JPEG"), LSM("LSM"), MAGNETOM_VISION("MAGNETOM VISION"), MEDIVISION("MEDIVISION"), MGH("MGH"), MICRO_CAT("MICRO CAT"),
	MINC("MINC"), MIPAV_XML("MIPAV XML"), MRC("MRC"), NIFTI("NIFTI"), NRRD("NRRD"), OSM("OSM"),
	PCX("PCX"), PIC("PIC"), PICT("PICT"), PNG("PNG"), QT("QT"), RAW("RAW"),
	SPM("SPM"), STK("STK"), TIFF("TIFF"), TGA("TGA"), TMG("TMG"), XBM("XBM"),
	XPM("XPM"), PARREC("PARREC"), MINC_HDF("MINC HDF"), LIFF("LIFF"), BFLOAT("BFLOAT"), SIEMENS_TEXT("SIEMENS TEXT"),
	ZVI("ZVI"), JP2("JP2"), MATLAB("MATLAB"), VISTA("VISTA"), ECAT6("ecat6"), ECAT7("ecat7"),
	PAR_REC("PAR/REC"), FIF("FIF")
	;

	private final String displayName;
	
	@Override
	public ValueInfo getValueInfo() {
		return new ValueInfo(this.toString(), this.getDisplayName());
	}
	
}
