package org.nrg.ccf.ndatransfer.constants;

import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum NdaScanType implements ValueInfoI {
	MR_DIFFUSION("MR diffusion"), FMRI("fMRI"), MR_STRUCTURAL_MPRAGE("MR structural (MPRAGE)"), MR_STRUCTURAL_T1("MR structural (T1)"),
	MR_STRUCTURAL_PD("MR structural (PD)"), MR_STRUCTURAL_FSPGR("MR structural (FSPGR)"), MR_STRUCTURAL_FISP("MR structural (FISP)"), 
	MR_STRUCTURAL_T2("MR structural (T2)"), PET("PET"), ASL("ASL"), MICROSCOPY("microscopy"), MR_STRUCTURAL_PDT2("MR structrual (PD,T2)"),
	MR_STRUCTURAL_B0MAP("MR structrual (B0,MAP)"), MR_STRUCTURAL_B1MAP("MR structrual (B1,MAP)"), 
	SINGLE_SHELL_DTI("single-shell DTI"), MULTI_SHELL_DTI("multi-shell DTI"),
	FIELD_MAP("Field Map"), XRAY("X-Ray"), STATIC_MAGNETIC_FIELD_B0("static magnetic field (B0)"), 
	PCASL_ASL("pCASL: ASL"), MR_T2STAR("MR: T2star"), MR_FLAIR("MR: FLAIR"), LOCALIZER("Localizer scan"),
	MR_STRUCTURAL_FLASH("MR structural (FLASH)"), MR_STRUCTURAL_MP2RAGE("MR structural (MP2RAGE)"),
	MR_STRUCTURAL_TSE("MR structural (TSE)"), MR_STRUCTURAL_T1T2("MR structural (T1, T2)"), MRS("Magnetic Resonance Spectroscopy(MRS)")
	//MEG("Magnetoencephalography (MEG)")
	;

	private final String displayName;

	@Override
	public ValueInfo getValueInfo() {
		return new ValueInfo(this.toString(), this.getDisplayName());
	}

}
