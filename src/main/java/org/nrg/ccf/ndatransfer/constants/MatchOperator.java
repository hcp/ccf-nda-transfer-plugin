package org.nrg.ccf.ndatransfer.constants;

import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum MatchOperator implements ValueInfoI {
	
	GREATER_THAN(" > "), GREATER_THAN_EQUALS(" >= "), EQUALS(" = "), LESS_THAN_EQUALS(" <= "), LESS_THAN(" < ")
	;

	private final String displayName;
	
	@Override
	public ValueInfo getValueInfo() {
		return new ValueInfo(this.toString(), this.getDisplayName());
	}
	
}
