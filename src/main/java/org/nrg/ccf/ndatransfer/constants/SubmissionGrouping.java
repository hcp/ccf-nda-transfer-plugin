package org.nrg.ccf.ndatransfer.constants;

import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum SubmissionGrouping implements ValueInfoI {
	
	SEPARATE("Separate - Each packages sent separately"), SESSION("Sessions - All packages per session"),
	PACKAGE("Pipeline - All subjects for a package"), COMBINED("Combined - Everything in one submission")
	;

	private final String displayName;
	
	@Override
	public ValueInfo getValueInfo() {
		return new ValueInfo(this.toString(), this.getDisplayName());
	}
	
}
