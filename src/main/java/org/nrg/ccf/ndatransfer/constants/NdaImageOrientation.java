package org.nrg.ccf.ndatransfer.constants;

import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum NdaImageOrientation implements ValueInfoI {
	
	AXIAL("Axial"), CORONAL("Coronal"), SAGITTAL("Sagittal")
	;

	private final String displayName;
	
	@Override
	public ValueInfo getValueInfo() {
		return new ValueInfo(this.toString(), this.getDisplayName());
	}
	
}
