package org.nrg.ccf.ndatransfer.constants;

import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum NdaModality implements ValueInfoI {
	
	MRI("MRI"), PET("PET"), CT("CT"), SPECT("SPECT"), ULTRASOUND("ultrasound"), FA("FA"), XRAY("X-Ray"), 
	SPECTROSCOPY("spectroscopy"), MICROSCOPY("microscopy"), ASL("ASL"), MEG("MET")
	;

	private final String displayName;
	
	@Override
	public ValueInfo getValueInfo() {
		return new ValueInfo(this.toString(), this.getDisplayName());
	}
	
}
