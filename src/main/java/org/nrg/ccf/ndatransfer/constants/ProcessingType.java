package org.nrg.ccf.ndatransfer.constants;

import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;

import com.google.common.base.CaseFormat;

public enum ProcessingType implements ValueInfoI {
	
	VALIDATE_AND_UPLOAD, VALIDATE_ONLY, PACKAGE_SIZE_REPORT, CINAB_STRUCTURE, CINAB_ARCHIVE;

	@Override
	public String getDisplayName() {
		return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,this.toString());
	}

	@Override
	public ValueInfo getValueInfo() {
		return new ValueInfo(this.toString(), this.getDisplayName());
	}

}
