package org.nrg.ccf.ndatransfer.constants;

import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum NdaImageUnit implements ValueInfoI {
	
	INCHES("Inches"), CENTIMETERS("Centimeters"), ANGSTROMS("Angstroms"), NANOMETERS("Nanometers"), 
	MICROMETERS("Micrometers"), MILLIMETERS("Millimeters"), METERS("Meters"), KILOMETERS("Kilometers"),
	MILES("Miles"), NANOSECONDS("Nanoseconds"), MICROSECONDS("Microseconds"), MILLISECONDS("Milliseconds"),
	SECONDS("Seconds"), MINUTES("Minutes"), HOURS("Hours"), HERTZ("Hertz"), FRAME_NUMBER("frame number"),
	DIFFUSION_GRADIENT("Diffusion gradient"),
	UNKNOWN("")
	;

	private final String displayName;
	
	@Override
	public ValueInfo getValueInfo() {
		return new ValueInfo(this.toString(), this.getDisplayName());
	}
	
}
