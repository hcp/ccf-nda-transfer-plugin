package org.nrg.ccf.ndatransfer.constants;

import org.nrg.ccf.ndatransfer.abst.NdaDataTypeI;
import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum NdaManifestType implements NdaDataTypeI, ValueInfoI {
	
	IMAGING_COLLECTION_01("imagingcollection01"),FMRI_RESULTS_01("fmriresults01"), IMAGE_03("image03")
	;
	
	private final String displayName;
	
	@Override
	public String getDisplayName() {
		return displayName;
	}

	@Override
	public ValueInfo getValueInfo() {
		return new ValueInfo(this.toString(), this.getDisplayName());
	}

}
