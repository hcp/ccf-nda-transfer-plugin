package org.nrg.ccf.ndatransfer.entities;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.AccessLevel;

@Getter
@Setter
public class PcpHelperInfo {
	
	private List<String> prereqCheckers;
	private List<String> validators;
	@Setter(AccessLevel.NONE)
	private Map<String,List<String>> componentConfig = new HashMap<>();

}
