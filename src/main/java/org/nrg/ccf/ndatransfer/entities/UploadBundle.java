package org.nrg.ccf.ndatransfer.entities;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UploadBundle {
	
	private String projectId = "";
	private String exportCsv = "";
	@Setter(AccessLevel.NONE)
	private final List<String> manifestDirs = new ArrayList<>();
	@Setter(AccessLevel.NONE)
	private final List<String> datafileDirs = new ArrayList<>();
	private String submissionTitle = "";
	private String submissionDesc = "";
	private Integer collectionId = Integer.MIN_VALUE;
	
	@Override
	public String toString() {
		return "UploadBundle [projectId=" + projectId + ", exportCsv=" + exportCsv + ", manifestDirs=" + manifestDirs
				+ ", datafileDirs=" + datafileDirs + ", submissionTitle=" + submissionTitle + ", submissionDesc="
				+ submissionDesc + ", collectionId=" + collectionId + "]";
	}
	
}
