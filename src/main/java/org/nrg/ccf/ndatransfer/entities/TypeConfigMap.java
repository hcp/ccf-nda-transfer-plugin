package org.nrg.ccf.ndatransfer.entities;

import java.util.HashMap;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.NdaDataTypeI;
import org.springframework.stereotype.Component;

@Component
public class TypeConfigMap extends HashMap<NdaDataTypeI,DataPackageDefinition> {

	private static final long serialVersionUID = -4862031365723568652L;

}
