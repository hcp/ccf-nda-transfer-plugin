package org.nrg.ccf.ndatransfer.entities;

import java.util.HashMap;
import java.util.List;

import lombok.Getter;

@Getter
public class EntityConfigs {
	
	@SuppressWarnings("serial")
	public class DataPackageDefinitionConfig extends HashMap<String,List<String>> {}
	@SuppressWarnings("serial")
	public class FileSelectorConfig extends HashMap<String,List<String>> {}
	@SuppressWarnings("serial")
	public class FileMatcherConfig extends HashMap<String,List<String>> {}
	
	public final DataPackageDefinitionConfig dataPackageDefinitionConfig = new DataPackageDefinitionConfig();
	public final FileSelectorConfig fileSelectorConfig = new FileSelectorConfig();
	public final FileMatcherConfig fileMatcherConfig = new FileMatcherConfig();
	
}
