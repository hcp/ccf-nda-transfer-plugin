package org.nrg.ccf.ndatransfer.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
@Table(indexes = { @Index(name = "IDX_NDASUBMISSIONITEM_ENTITYID", columnList = "entityId") })
public class NdaSubmissionItem extends AbstractHibernateEntity {

	private static final long serialVersionUID = -8298784987153742216L;
	private String entityId;
	private String entityType;
	private String itemId;
	private NdaSubmissionEntity ndaSubmissionEntity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "submissionentity_id")
	public NdaSubmissionEntity getNdaSubmissionEntity() {
		return ndaSubmissionEntity;
	}

	public void setNdaSubmissionEntity(NdaSubmissionEntity ndaSubmissionEntity) {
		this.ndaSubmissionEntity = ndaSubmissionEntity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
		result = prime * result + ((entityType == null) ? 0 : entityType.hashCode());
		result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
		result = prime * result + ((ndaSubmissionEntity == null) ? 0 : ndaSubmissionEntity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NdaSubmissionItem other = (NdaSubmissionItem) obj;
		if (entityId == null) {
			if (other.entityId != null)
				return false;
		} else if (!entityId.equals(other.entityId))
			return false;
		if (entityType == null) {
			if (other.entityType != null)
				return false;
		} else if (!entityType.equals(other.entityType))
			return false;
		if (itemId == null) {
			if (other.itemId != null)
				return false;
		} else if (!itemId.equals(other.itemId))
			return false;
		if (ndaSubmissionEntity == null) {
			if (other.ndaSubmissionEntity != null)
				return false;
		} else if (!(ndaSubmissionEntity.getId() == other.ndaSubmissionEntity.getId()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NdaSubmissionItem [entityId=" + entityId + ", entityType=" + entityType + ", itemId=" + itemId
				+ ", ndaSubmissionEntity=" + ndaSubmissionEntity + "]";
	}
	
}
