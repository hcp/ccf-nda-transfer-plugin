package org.nrg.ccf.ndatransfer.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Entity
@Getter
@Setter
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "projectId", "submissionDatasetTitle" }))
public class NdaSubmissionEntity extends AbstractHibernateEntity {

	private static final long serialVersionUID = -3026483131466475150L;
	private String submissionDatasetTitle;
	private String submissionDatasetDescription;
	private String projectId;
	private Integer collectionId;
	private Integer submissionId;
	private String submissionStatus;
	private String tempDirectory;
	private final List<NdaSubmissionItem> ndaSubmissionItems = new ArrayList<>();
	
	@OneToMany(targetEntity = NdaSubmissionItem.class,
				mappedBy = "ndaSubmissionEntity",
				fetch = FetchType.LAZY)
			   //cascade = CascadeType.ALL)
	public List<NdaSubmissionItem> getNdaSubmissionItems() {
		return ndaSubmissionItems;
	}
	
	public void setNdaSubmissionItems(List<NdaSubmissionItem> ndaSubmissionItems) {
		if (ndaSubmissionItems == null) {
			return;
		}
		this.ndaSubmissionItems.clear();
		try {
			this.ndaSubmissionItems.addAll(ndaSubmissionItems);
		} catch (Throwable t) {
			log.error(ExceptionUtils.getFullStackTrace(t));
			
		}
	}
	
	public void addNdaSubmissionItem(NdaSubmissionItem ndaSubmissionItem) {
		ndaSubmissionItem.setNdaSubmissionEntity(this);
	}
	
	public void removeNdaSubmissionItem(NdaSubmissionItem ndaSubmissionItem) {
		this.ndaSubmissionItems.remove(ndaSubmissionItem);
		ndaSubmissionItem.setNdaSubmissionEntity(null);
	}

	@Override
	public String toString() {
		return "NdaSubmissionEntity [submissionDatasetTitle=" + submissionDatasetTitle +
				", submissionDatasetDescription=" + submissionDatasetDescription +
				", projectId=" + projectId
				+ ", collectionId=" + collectionId + ", submissionId=" + submissionId + ", submissionStatus="
				+ submissionStatus + ", ndaSubmissionItems=" + ndaSubmissionItems + "]";
	}

}
