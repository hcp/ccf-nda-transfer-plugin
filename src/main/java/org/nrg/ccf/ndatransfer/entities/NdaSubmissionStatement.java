package org.nrg.ccf.ndatransfer.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "nrg")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "tempDirectory" }))
public class NdaSubmissionStatement extends AbstractHibernateEntity {

	private static final long serialVersionUID = -2305851482403485538L;
	private String tempDirectory;
	private String submissionStatement;

	public NdaSubmissionStatement() {
		super();
	}

	public NdaSubmissionStatement(String tempDirectory) {
		super();
		this.tempDirectory = tempDirectory;
	}

	public NdaSubmissionStatement(String tempDirectory, String submissionStatement) {
		super();
		this.tempDirectory = tempDirectory;
		this.submissionStatement = submissionStatement;
	}

	public String getTempDirectory() {
		return tempDirectory;
	}

	public void setTempDirectory(String tempDirectory) {
		this.tempDirectory = tempDirectory;
	}

	@Column(length=2040)
	public String getSubmissionStatement() {
		return submissionStatement;
	}

	public void setSubmissionStatement(String submissionStatement) {
		this.submissionStatement = submissionStatement;
	}
	
	@Override
	public String toString() {
		return "NdaSubmissionStatement [tempDirectory=" + tempDirectory +
				", submissionStatement=" + submissionStatement + "]";
	}

}

