package org.nrg.ccf.ndatransfer.dao;

import org.nrg.ccf.ndatransfer.entities.NdaSubmissionStatement;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;

@Repository
public class NdaSubmissionStatementDAO extends AbstractHibernateDAO<NdaSubmissionStatement> {

}
