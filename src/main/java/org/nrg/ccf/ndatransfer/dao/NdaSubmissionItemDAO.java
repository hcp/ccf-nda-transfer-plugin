package org.nrg.ccf.ndatransfer.dao;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.hibernate.Query;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionItem;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class NdaSubmissionItemDAO extends AbstractHibernateDAO<NdaSubmissionItem> {

	public NdaSubmissionItem getItemForStatusEntity(PcpStatusEntity statusEntity) {
		try {
			final Query query = getSession().createQuery(
					"SELECT submissionItem "
					+ " FROM NdaSubmissionItem as submissionItem INNER JOIN FETCH submissionItem.ndaSubmissionEntity " +
			          " WHERE submissionItem.entityId in (:entityId,:entityLabel) and " +
					  "submissionItem.entityType = :entityType and submissionItem.itemId = :itemId ORDER BY submissionItem.created DESC"
					).setParameter("entityId",statusEntity.getEntityId()).setParameter("entityType",statusEntity.getEntityType())
					    .setParameter("entityLabel",statusEntity.getEntityLabel()).setParameter("itemId",statusEntity.getSubGroup());
			query.setMaxResults(1);
			Object result = query.uniqueResult();
			return (NdaSubmissionItem)result;
		} catch (Throwable e) {
			log.error(ExceptionUtils.getFullStackTrace(e));
			throw e;
		}
	}

}
