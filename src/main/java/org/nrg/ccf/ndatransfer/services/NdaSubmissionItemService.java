package org.nrg.ccf.ndatransfer.services;

import org.nrg.ccf.ndatransfer.dao.NdaSubmissionItemDAO;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionItem;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NdaSubmissionItemService extends AbstractHibernateEntityService<NdaSubmissionItem, NdaSubmissionItemDAO> {
	
	@Transactional
	public NdaSubmissionItem getItemForStatusEntity(PcpStatusEntity statusEntity) {
		return getDao().getItemForStatusEntity(statusEntity);
	}

}
