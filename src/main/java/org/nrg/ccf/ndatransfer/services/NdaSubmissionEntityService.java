package org.nrg.ccf.ndatransfer.services;

import org.nrg.ccf.ndatransfer.dao.NdaSubmissionEntityDAO;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionEntity;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.springframework.stereotype.Service;

@Service
public class NdaSubmissionEntityService extends AbstractHibernateEntityService<NdaSubmissionEntity, NdaSubmissionEntityDAO> {

}
