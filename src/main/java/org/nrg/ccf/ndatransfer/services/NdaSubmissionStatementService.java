package org.nrg.ccf.ndatransfer.services;

import org.nrg.ccf.ndatransfer.dao.NdaSubmissionStatementDAO;
import org.nrg.ccf.ndatransfer.entities.NdaSubmissionStatement;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NdaSubmissionStatementService extends AbstractHibernateEntityService<NdaSubmissionStatement, NdaSubmissionStatementDAO> {
	

    @Transactional
    public NdaSubmissionStatement findByTempDir(final String tempDirectory) {
        return getDao().findByUniqueProperty("tempDirectory", tempDirectory);
    }

    @Transactional
	public NdaSubmissionStatement getOrCreateByTempDir(String tempDirectory) {
		final NdaSubmissionStatement statement = findByTempDir(tempDirectory);
		return (statement != null) ? statement : new NdaSubmissionStatement(tempDirectory);
	}

}
