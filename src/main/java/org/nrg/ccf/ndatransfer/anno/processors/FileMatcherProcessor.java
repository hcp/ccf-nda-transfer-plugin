package org.nrg.ccf.ndatransfer.anno.processors;

import org.kohsuke.MetaInfServices;
import com.google.common.collect.Maps;

import org.nrg.ccf.ndatransfer.anno.FileMatcher;
import org.nrg.framework.processors.NrgAbstractAnnotationProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.util.Map;

@MetaInfServices(Processor.class)
@SupportedAnnotationTypes("org.nrg.ccf.ndatransfer.anno.FileMatcher")
public class FileMatcherProcessor extends NrgAbstractAnnotationProcessor<FileMatcher> {
	
	@Override
	protected Map<String, String> processAnnotation(TypeElement element, FileMatcher annotation) {
		final Map<String, String> properties = Maps.newLinkedHashMap();
		properties.put(FileMatcher.FILE_MATCHER, element.getQualifiedName().toString());
		return properties;
	}

	@Override
	protected String getPropertiesName(TypeElement element, FileMatcher annotation) {
        return String.format("ndatransfer/%s-ndatransfer.properties", element.getSimpleName());
	}

}
