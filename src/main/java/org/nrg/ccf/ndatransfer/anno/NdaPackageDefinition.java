package org.nrg.ccf.ndatransfer.anno;

public @interface NdaPackageDefinition {

	String NDA_PACKAGE_DEFINITION = "NdaPackageDefnition";
	
	String description() default "";

}
