package org.nrg.ccf.ndatransfer.anno.processors;

import org.kohsuke.MetaInfServices;
import com.google.common.collect.Maps;

import org.nrg.ccf.ndatransfer.anno.NdaPackageDefinition;
import org.nrg.framework.processors.NrgAbstractAnnotationProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.util.Map;

@MetaInfServices(Processor.class)
@SupportedAnnotationTypes("org.nrg.ccf.ndatransfer.anno.NdaPackageDefinition")
public class NdaPackageDefinitionProcessor extends NrgAbstractAnnotationProcessor<NdaPackageDefinition> {
	
	@Override
	protected Map<String, String> processAnnotation(TypeElement element, NdaPackageDefinition annotation) {
		final Map<String, String> properties = Maps.newLinkedHashMap();
		properties.put(NdaPackageDefinition.NDA_PACKAGE_DEFINITION, element.getQualifiedName().toString());
		return properties;
	}

	@Override
	protected String getPropertiesName(TypeElement element, NdaPackageDefinition annotation) {
        return String.format("ndatransfer/%s-ndatransfer.properties", element.getSimpleName());
	}

}
