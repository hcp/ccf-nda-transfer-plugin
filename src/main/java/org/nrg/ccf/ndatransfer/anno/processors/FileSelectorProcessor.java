package org.nrg.ccf.ndatransfer.anno.processors;

import org.kohsuke.MetaInfServices;
import com.google.common.collect.Maps;

import org.nrg.ccf.ndatransfer.anno.FileSelector;
import org.nrg.framework.processors.NrgAbstractAnnotationProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.util.Map;

@MetaInfServices(Processor.class)
@SupportedAnnotationTypes("org.nrg.ccf.ndatransfer.anno.FileSelector")
public class FileSelectorProcessor extends NrgAbstractAnnotationProcessor<FileSelector> {
	
	@Override
	protected Map<String, String> processAnnotation(TypeElement element, FileSelector annotation) {
		final Map<String, String> properties = Maps.newLinkedHashMap();
		properties.put(FileSelector.FILE_SELECTOR, element.getQualifiedName().toString());
		return properties;
	}

	@Override
	protected String getPropertiesName(TypeElement element, FileSelector annotation) {
        return String.format("ndatransfer/%s-ndatransfer.properties", element.getSimpleName());
	}

}
