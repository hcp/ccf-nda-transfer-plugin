package org.nrg.ccf.ndatransfer.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface NdaPackagePrereqChecker {

	String NDA_PACKAGE_PREREQ_CHECKER = "NdaPackagePrereqChecker";
	
	String description() default "";

}
