package org.nrg.ccf.ndatransfer.xapi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.nrg.ccf.common.utilities.components.XnatUtils;
import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;
import org.nrg.ccf.ndatransfer.client.entities.ProjectTransferSettings;
import org.nrg.ccf.ndatransfer.client.entities.SiteTransferSettings;
import org.nrg.ccf.ndatransfer.client.entities.TransferSettings;
import org.nrg.ccf.ndatransfer.entities.EntityConfigs;
import org.nrg.ccf.ndatransfer.entities.PcpHelperInfo;
import org.nrg.ccf.ndatransfer.utils.NdaTransferConfigUtils;
import org.nrg.ccf.ndatransfer.utils.EntityHelpers;
import org.nrg.ccf.ndatransfer.utils.PcpHelpers;
import org.nrg.ccf.ndatransfer.utils.ValueInfoUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.framework.exceptions.NrgServiceException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
//import lombok.extern.slf4j.Slf4j;

//@Slf4j
@Lazy
@XapiRestController
@Api(description = "Pipeline Control Panel Configuration API")
public class CcfNdaTransferConfigApi extends AbstractXapiRestController {
	
	private NdaTransferConfigUtils _configUtils;
	private XnatUtils _xnatUtils;
	private PcpHelpers _pcpHelpers;
	private EntityHelpers _entityHelpers;
	private ProjectTransferSettings _projectTransferSettings;
	private SiteTransferSettings _siteTransferSettings;

	@Autowired
	@Lazy
	public CcfNdaTransferConfigApi(UserManagementServiceI userManagementService, RoleHolder roleHolder,
			NdaTransferConfigUtils configUtils, SiteTransferSettings siteTransferSettings, ProjectTransferSettings projectTransferSettings,
			PcpHelpers pcpHelpers, EntityHelpers entityHelpers, XnatUtils xnatUtils) {
		super(userManagementService, roleHolder);
		_configUtils = configUtils;
		_siteTransferSettings = siteTransferSettings;
		_projectTransferSettings = projectTransferSettings;
		_pcpHelpers = pcpHelpers;
		_entityHelpers = entityHelpers;
		_xnatUtils = xnatUtils;
	}
	
	@ApiOperation(value = "Gets site transfer settings", notes = "Returns site-level transfer settings",
			response = TransferSettings.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransferConfig/transferSettings"}, restrictTo=AccessLevel.Authenticated, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<TransferSettings> getTransferSettings() throws NrgServiceException {
		
		return new ResponseEntity<TransferSettings>(_siteTransferSettings.getTransferSettings(),HttpStatus.OK);
	
    }
	
	@ApiOperation(value = "Sets site transfer settings", notes = "Sets site-level transfer settings",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransferConfig/transferSettings"}, restrictTo=AccessLevel.Admin, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setTransferSettings(@RequestBody final TransferSettings settings) throws NrgServiceException {
		
		_siteTransferSettings.setNdaApiUrl(settings.getNdaApiUrl());
		_siteTransferSettings.setPathToVtcmd(settings.getPathToVtcmd());
		return new ResponseEntity<>(HttpStatus.OK);
	
    }
	
	@ApiOperation(value = "Gets project transfer settings", notes = "Returns NDA transfer plugin project-level settings.",
			response = TransferSettings.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransferConfig/{projectId}/transferSettings"}, restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<TransferSettings> getTransferSettings(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		
		TransferSettings transferSettings = _projectTransferSettings.getTransferSettings(projectId); 
		if ((transferSettings.getNdaApiUrl() == null || transferSettings.getNdaApiUrl().isEmpty()) && 
				(transferSettings.getPathToVtcmd() == null || transferSettings.getPathToVtcmd().isEmpty())) {
			transferSettings = _siteTransferSettings.getTransferSettings();
		}
		return new ResponseEntity<TransferSettings>(transferSettings,HttpStatus.OK);
		
    }
	
	@ApiOperation(value = "Sets project transfer settings", notes = "Sets project-level transfer settings",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransferConfig/{projectId}/transferSettings"}, restrictTo=AccessLevel.Owner, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setTransferSettings(@PathVariable("projectId") @ProjectId final String projectId, @RequestBody final TransferSettings settings) throws NrgServiceException {
		
		_projectTransferSettings.setNdaApiUrl(projectId, settings.getNdaApiUrl());
		_projectTransferSettings.setPathToVtcmd(projectId, settings.getPathToVtcmd());
		_projectTransferSettings.setDefaultCollectionId(projectId, settings.getDefaultCollectionId());
		_projectTransferSettings.setTimeoutMinutes(projectId, settings.getTimeoutMinutes());
		_projectTransferSettings.setTempdirLocation(projectId, settings.getTempdirLocation());
		return new ResponseEntity<>(HttpStatus.OK);
	
    }
	
	@ApiOperation(value = "Gets project configured packages", notes = "Returns NDA transfer plugin project-level configuration.",
			response = DataPackageDefinition.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransferConfig/{projectId}/dataPackages"}, restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<DataPackageDefinition>> getConfig(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		
		return new ResponseEntity<List<DataPackageDefinition>>(_configUtils.getDataPackageDefinitions(projectId, false),HttpStatus.OK);
	
    }
	
	@ApiOperation(value = "Sets project configured packages", notes = "Sets NDA transfer plugin project-level configuration.",
			response = Void.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransferConfig/{projectId}/dataPackages"}, restrictTo=AccessLevel.Member, consumes = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Void> setConfig(@PathVariable("projectId") @ProjectId final String projectId, @RequestBody final List<? extends DataPackageDefinition> dataPackageDefinitions) throws NrgServiceException {
		
		_configUtils.setDataPackageDefinitions(projectId, dataPackageDefinitions, getSessionUser());
		return new ResponseEntity<Void>(HttpStatus.OK);
	
    }
	
	@ApiOperation(value = "Gets project value lists from enums", notes = "Returns NDA transfer plugin configuration value lists.",
			response = String.class, responseContainer = "Map")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransferConfig/valueLists"}, restrictTo=AccessLevel.Authenticated,  produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Map<String,List<ValueInfo>>> getEnumMaps() throws NrgServiceException {
		
		final Map<String,List<ValueInfo>> enumMap = new HashMap<>();
		final Reflections reflections = new Reflections("org.nrg.ccf.ndatransfer");
		final Set<Class<? extends ValueInfoI>> classes = reflections.getSubTypesOf(ValueInfoI.class);
		for (final Class<? extends ValueInfoI> clazz : classes) {
			enumMap.put(clazz.getSimpleName(), ValueInfoUtils.getValueInfoList(clazz));
		}
		return new ResponseEntity<Map<String,List<ValueInfo>>>(enumMap,HttpStatus.OK);
    }
	
	@ApiOperation(value = "Gets entity configuration YAML", notes = "Returns entity configuration YAML.",
			response = EntityConfigs.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransferConfig/entityConfigs"}, restrictTo=AccessLevel.Authenticated, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<EntityConfigs> getEntityConfigs() throws NrgServiceException {
		
		return new ResponseEntity<EntityConfigs>(_entityHelpers.getEntityConfigs(),HttpStatus.OK);
		
    }
	
	@ApiOperation(value = "Gets PCP Helper Info", notes = "Returns NDA transfer plugin PCP helper information.",
			response = PcpHelperInfo.class)
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransferConfig/getPcpHelperInfo"}, restrictTo=AccessLevel.Authenticated, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<PcpHelperInfo> getPcpHelperInfo() throws NrgServiceException {
		
		return new ResponseEntity<PcpHelperInfo>(_pcpHelpers.getPcpHelperInfo(),HttpStatus.OK);
		
    }
	
	@ApiOperation(value = "Gets project series descriptions", notes = "Gets project series descriptions.",
			response = String.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = {"/ccfNdaTransferConfig/{projectId}/getProjectSeriesDescriptions"}, restrictTo=AccessLevel.Read, produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<String>> getProjectSeriesDescriptions(@PathVariable("projectId") @ProjectId final String projectId) throws NrgServiceException {
		
		return new ResponseEntity<List<String>>(_xnatUtils.getImageSessionSeriesDescriptions(projectId),HttpStatus.OK);
		
    }

	
}
