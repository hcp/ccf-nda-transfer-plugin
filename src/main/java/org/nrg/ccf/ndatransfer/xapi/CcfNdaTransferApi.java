package org.nrg.ccf.ndatransfer.xapi;

import java.util.Map;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.List;
import java.io.File;
import java.util.ArrayList;

import org.nrg.ccf.common.utilities.utils.ExperimentUtils;
import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.abst.ManifestPackageDefinition;
import org.nrg.ccf.ndatransfer.pojo.FileInfo;
import org.nrg.ccf.ndatransfer.pojo.SessionFileList;
import org.nrg.ccf.ndatransfer.utils.NdaTransferConfigUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.ProjectId;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.security.helpers.AccessLevel;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.search.CriteriaCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Lazy
@XapiRestController
@Api(description = "NDA Transfer API")
public class CcfNdaTransferApi extends AbstractXapiRestController {

    private NdaTransferConfigUtils configUtils;

    @Autowired
    @Lazy
    public CcfNdaTransferApi(
        UserManagementServiceI umgmtService,
        RoleHolder rHolder,
        NdaTransferConfigUtils configUtils)
    {
        super(umgmtService, rHolder);
        this.configUtils = configUtils;
    }

    @ApiOperation(
        value = "List available packages",
        notes = "Return a list of packages for project",
        response = List.class)
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "No such project or packages not configured for project"),
        @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(
        value = {"/ccfNdaTransfer/{projectId}/packages"},
        restrictTo = AccessLevel.Read,
        produces = {MediaType.APPLICATION_JSON_VALUE},
        method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<String>> getPackageList(
        @PathVariable("projectId") @ProjectId final String projectId)
    {
        final List<String> packageIds = getPackageIds(configUtils.getDataPackageDefinitions(projectId, false));
        if (packageIds == null || packageIds.isEmpty()) {
        	return new ResponseEntity<List<String>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<String>>(packageIds,HttpStatus.OK);
    }

    @ApiOperation(
        value = "Get project file list",
        notes = "Return project level file lists",
        response = List.class)
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 400, message = "Requested package does not exist"),
        @ApiResponse(code = 404, message = "No such project or packages not configured for project"),
        @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(
        value = {"/ccfNdaTransfer/{projectId}/packages/{packageIds}/files"},
        restrictTo = AccessLevel.Read,
        produces = {MediaType.APPLICATION_JSON_VALUE},
        method = RequestMethod.GET)
    @ResponseBody
    //public ResponseEntity<Map<String, List<File>>> getFileList(
    public ResponseEntity<List<FileInfo>> getFileList(
        @PathVariable("projectId") @ProjectId final String projectId,
        @PathVariable("packageIds") final List<String> packageIds)
    {
        //List<String> packageIds = Arrays.asList(new String[]{packageId,});
        //return mm_getFileList(projectId, packageIds, null);
        final List<DataPackageDefinition> packageDefs = configUtils.getDataPackageDefinitions(projectId, false);
        if (packageDefs == null || packageDefs.isEmpty()) {
        	return new ResponseEntity<List<FileInfo>>(HttpStatus.NOT_FOUND);
        }
        for (final String packageId : packageIds) {
        	if ( !getPackageIds(packageDefs).contains(packageId)) {
        		return new ResponseEntity<List<FileInfo>>(HttpStatus.BAD_REQUEST);
        	}
        }
        final List<FileInfo> fileList = getFileInfo(mm_getFileList(projectId,getPackageDefs(packageDefs, packageIds)));
    	return new ResponseEntity<List<FileInfo>>(fileList,HttpStatus.OK);
    }

    @ApiOperation(
        value = "Get project file list",
        notes = "Return project level file lists",
        response = List.class)
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 400, message = "Requested package does not exist"),
        @ApiResponse(code = 404, message = "No such project or packages not configured for project"),
        @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(
        value = {"/ccfNdaTransfer/{projectId}/packages/{packageIds}/sessions/{sessionIds}/files"},
        restrictTo = AccessLevel.Read,
        produces = {MediaType.APPLICATION_JSON_VALUE},
        method = RequestMethod.GET)
    @ResponseBody
    //public ResponseEntity<Map<String, List<File>>> getFileList(
    public ResponseEntity<List<FileInfo>> getFileList(
        @PathVariable("projectId") @ProjectId final String projectId,
        @PathVariable("packageIds") final List<String> packageIds,
        @PathVariable("sessionIds") final List<String> sessionIds)
    {
        //List<String> packageIds = Arrays.asList(new String[]{packageId,});
        //return mm_getFileList(projectId, packageIds, sessionIds);
    	//return new ResponseEntity<Map<String, List<File>>>(new HashMap(),HttpStatus.OK):
        final List<DataPackageDefinition> packageDefs = configUtils.getDataPackageDefinitions(projectId, false);
        if (packageDefs == null || packageDefs.isEmpty()) {
        	return new ResponseEntity<List<FileInfo>>(HttpStatus.NOT_FOUND);
        }
        for (final String packageId : packageIds) {
        	if ( !getPackageIds(packageDefs).contains(packageId)) {
        		return new ResponseEntity<List<FileInfo>>(HttpStatus.BAD_REQUEST);
        	}
        }
        final List<FileInfo> fileList = getFileInfo(mm_getFileList(projectId,getPackageDefs(packageDefs, packageIds), sessionIds));
    	return new ResponseEntity<List<FileInfo>>(fileList,HttpStatus.OK);
    }

    private List<DataPackageDefinition> getPackageDefs(List<DataPackageDefinition> dataPackageDefinitions, List<String> packageIds) {
    	final List<DataPackageDefinition> returnList = new ArrayList<>();
    	for (final String packageId : packageIds) {
	    	for (final DataPackageDefinition packageDef : dataPackageDefinitions) {
	    		if (packageDef.getPackageGroup().equals(packageId)) {
	    			returnList.add(packageDef);
	    		};
	        }
        }
    	return returnList;
	}

	private List<String> getPackageIds(final List<DataPackageDefinition> dataPackageDefinitions) {
    	final List<String> returnList = new ArrayList<>();
    	for (final DataPackageDefinition packageDef : dataPackageDefinitions) {
    		final String packageGroup = packageDef.getPackageGroup();
    		// Let's exclude any CinaB or Image03 packages
    		if (packageGroup.toUpperCase().contains("CINAB") || packageGroup.toUpperCase().contains("IMAGE03")) {
    			continue;
    		}
    		returnList.add(packageDef.getPackageGroup());
        }
    	return returnList;
	}

    private String getUriFromFile(String projectId, XnatImagesessiondata imageSession, File fl) {
    	final StringBuilder sb = new StringBuilder("/data/projects/");
    	sb.append(projectId).append("/subjects/").append(imageSession.getSubjectId())
    		.append("/experiments/").append(imageSession.getLabel()).append("/resources/");
    	final String postResource = fl.getPath().replaceFirst("^.*[/]RESOURCES[/]", "");
    	final String resource = postResource.replaceFirst("[/].*$", "");
    	final String filePath = postResource.replaceFirst("^[^/]*[/]", "");
    	sb.append(resource);
    	sb.append("/files/");
    	sb.append(filePath);
		return sb.toString();
	}
	
	private List<FileInfo> getFileInfo(Map<String, SessionFileList> fileMap) {
		List<FileInfo> returnList = new ArrayList<>();
   		for (Entry<String, SessionFileList> entry : fileMap.entrySet()) {
   			final SessionFileList sfl = entry.getValue();
   			for (final File fl : sfl.getFileList()) {
   				final String fileURI = getUriFromFile(sfl.getProjectId(), sfl.getImageSession(), fl);
   				if (fileURI != null) {
   					returnList.add(new FileInfo(entry.getKey(), fileURI));
   				}
   			}
   		}
		return returnList;
	}

	private Map<String, SessionFileList> mm_getFileList(String projectId, List<DataPackageDefinition> packageDefs) {
    	return mm_getFileList(projectId, packageDefs, null);
    }

    private Map<String, SessionFileList> mm_getFileList(String projectId, List<DataPackageDefinition> packageDefs, List<String> sessionIds) {
    	
    	final Map<String, SessionFileList> returnMap = new HashMap<>();
    	for (final DataPackageDefinition packageDef : packageDefs) {
	    	if (!(packageDef instanceof ManifestPackageDefinition)) {
	    		continue;
	    	}
	   		final ManifestPackageDefinition manifestDef = (ManifestPackageDefinition) packageDef;
	   		final List<XnatImagesessiondata> imageSessions = new ArrayList<>();;
	   		if (sessionIds == null) {
	   	    	final CriteriaCollection cc=new CriteriaCollection("OR");
	   	    	
	   	    	final CriteriaCollection subcc1 = new CriteriaCollection("AND");
	   	        subcc1.addClause("xnat:mrsessionData/project", projectId);
	   	        cc.add(subcc1);
	   	    	final CriteriaCollection subcc2 = new CriteriaCollection("AND");
	   	    	subcc2.addClause("xnat:mrsessionData/sharing/share/project", projectId);
	   	        cc.add(subcc2);
	   			imageSessions.addAll(XnatImagesessiondata.getXnatImagesessiondatasByField(cc, getSessionUser(), false));
	   			
	   		} else {
		    	for (String sessionId : sessionIds) {
		    		final XnatExperimentdataI exp = ExperimentUtils.getExperiment(projectId, sessionId, getSessionUser());
		    		if (exp == null || !(exp instanceof XnatImagesessiondata)) {
		    			continue;
		    		}
		    		imageSessions.add((XnatImagesessiondata)exp);
		    	}
	   		}
	    	for (final XnatImagesessiondata imageSession : imageSessions) {
	    		final XnatImagescandata imageScan = null;
	    		Map<String, List<File>> fileMap = manifestDef.getFileMap(getSessionUser(), imageSession, imageScan, true);
	    		for (Entry<String, List<File>> entry : fileMap.entrySet()) {
	    			final List<File> value = entry.getValue();
	    			if ((!returnMap.containsKey(entry.getKey())) && value != null && !value.isEmpty()) {
	    				returnMap.put(entry.getKey(), new SessionFileList(projectId,imageSession,entry.getValue()));
	    			}
	    		}
	    	}
    	}
    	return returnMap;
    }

}
