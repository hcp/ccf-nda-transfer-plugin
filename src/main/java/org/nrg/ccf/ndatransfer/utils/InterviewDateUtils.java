package org.nrg.ccf.ndatransfer.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.nrg.ccf.ndaguid.constants.NdaGuidConstants;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.om.base.BaseXnatExperimentdata;
import org.springframework.stereotype.Component;

@Component
public class InterviewDateUtils {
	
	final SimpleDateFormat _formatter = new SimpleDateFormat("MM/dd/yyyy");
	
	public String getInterviewDate(XnatExperimentdataI exp) {
		if (!(exp instanceof BaseXnatExperimentdata)) {
			return "";
		}
		final BaseXnatExperimentdata expData = (BaseXnatExperimentdata)exp;
		final Object ndaInterviewDateObj = expData.getFieldByName(NdaGuidConstants.INTERVIEW_DATE_FIELD);
		if (ndaInterviewDateObj == null) {
			return "";
		}
		if ((ndaInterviewDateObj instanceof Date)) {
			final Date ndaInterviewDate = (Date)ndaInterviewDateObj;
			return (ndaInterviewDate != null) ? _formatter.format(ndaInterviewDate) : "";  
		} else {
			final String ndaInterviewDateString = ndaInterviewDateObj.toString();
			try {
				final Date ndaInterviewDate = _formatter.parse(ndaInterviewDateString);
				return (ndaInterviewDate != null) ? ndaInterviewDateString : "";  
			} catch (ParseException e) {
				return "";
			}
		}
	}
	
}
