package org.nrg.ccf.ndatransfer.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.nrg.ccf.ndatransfer.abst.NdaPackagePrereqCheckerI;
import org.nrg.ccf.ndatransfer.abst.NdaPackageValidatorI;
import org.nrg.ccf.ndatransfer.anno.NdaPackagePrereqChecker;
import org.nrg.ccf.ndatransfer.anno.NdaPackageValidator;
import org.nrg.ccf.ndatransfer.entities.PcpHelperInfo;
import org.nrg.ccf.pcp.inter.ConfigurableComponentI;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class PcpHelpers {
	
	
	private List<Resource> _resourceList;
	final private List<Class<? extends NdaPackagePrereqCheckerI>> _packagePrereqCheckers = new ArrayList<>();
	final private List<Class<? extends NdaPackageValidatorI>> _packageValidators = new ArrayList<>();
	final private Map<String, List<String>> _componentConfig = new HashMap<>();
	final private String[] _componentArr = new String[] { NdaPackageValidator.NDA_PACKAGE_VALIDATOR, NdaPackagePrereqChecker.NDA_PACKAGE_PREREQ_CHECKER };
	
	@SuppressWarnings("unchecked")
	public PcpHelpers() {
		super();
		for (final String component : Arrays.asList(_componentArr)) {
			try {
				for (final Resource resource : getResourceList()) {
					final Properties properties = PropertiesLoaderUtils.loadProperties(resource);
					if (!properties.containsKey(component)) {
						continue;
					}
					final String className = properties.getProperty(component);
					final Class<?> clazz;
					try {
						clazz = Class.forName(className);
					} catch (ClassNotFoundException e) {
						log.debug("ERROR:  Class not found ({})",className);
						continue;
					}
					if (NdaPackageValidatorI.class.isAssignableFrom(clazz)) {
						_packageValidators.add((Class<? extends NdaPackageValidatorI>)clazz);
					} else if (NdaPackagePrereqCheckerI.class.isAssignableFrom(clazz)) {
						_packagePrereqCheckers.add((Class<? extends NdaPackagePrereqCheckerI>)clazz);
					}
					if (ConfigurableComponentI.class.isAssignableFrom(clazz)) {
						populateComponentConfig(className, (Class<? extends ConfigurableComponentI>)clazz);
					}
				}
			} catch (IOException e) {
				log.error("Exception thrown", e);
			}
		}
	}
    
	private void populateComponentConfig(final String className, final Class<? extends ConfigurableComponentI> clazz) {
		ConfigurableComponentI comp;
		try {
			comp = (ConfigurableComponentI)clazz.newInstance();
			final List<String> configYaml = comp.getConfigurationYaml();
			_componentConfig.put(className, configYaml);
		} catch (InstantiationException | IllegalAccessException e) {
			log.warn("WARNING:  Exception thrown populating configurableComponents map", e);
		}
	}

	private List<Resource> getResourceList() throws IOException {
		return (_resourceList!=null) ? _resourceList : BasicXnatResourceLocator.getResources("classpath*:META-INF/xnat/ndatransfer/*-ndatransfer.properties");
	}

	public PcpHelperInfo getPcpHelperInfo() {
		final PcpHelperInfo helperInfo = new PcpHelperInfo();
		helperInfo.setPrereqCheckers(this.getPackagePrereqCheckerNames());
		helperInfo.setValidators(this.getPackageValidatorNames());
		helperInfo.getComponentConfig().putAll(_componentConfig);
		return helperInfo;
	}
	
	public List<Class<? extends NdaPackagePrereqCheckerI>> getPackagePrereqCheckerClasses() {
		return _packagePrereqCheckers;
	}
	
	public List<String> getPackagePrereqCheckerNames() {
		final List<String> returnList = new ArrayList<>();
		for (final Class<? extends NdaPackagePrereqCheckerI> clazz : _packagePrereqCheckers) {
			returnList.add(clazz.getSimpleName());
		}
		return returnList;
	}
	
	public NdaPackagePrereqCheckerI getPackagePrereqCheckerInstance(final String simpleName) throws ReflectiveOperationException { 
		for (final Class<? extends NdaPackagePrereqCheckerI> clazz : _packagePrereqCheckers) {
			if (clazz.getSimpleName().equals(simpleName)) {
				try {
					return clazz.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					log.error("Error creating prereq checker instance:");
					log.error(ExceptionUtils.getStackTrace(e));
					throw e;
				}
			}
		}
		log.error("ERROR:  Requested prereq checker not found ({})",simpleName);
		return null;
	}
	
	public List<Class<? extends NdaPackageValidatorI>> getPackageValidatorClasses() {
		return _packageValidators;
	}
	
	public List<String> getPackageValidatorNames() {
		final List<String> returnList = new ArrayList<>();
		for (final Class<? extends NdaPackageValidatorI> clazz : _packageValidators) {
			returnList.add(clazz.getSimpleName());
		}
		return returnList;
	}
	
	public NdaPackageValidatorI getPackageValidatorInstance(final String simpleName) 
			throws InstantiationException, IllegalAccessException {
		for (final Class<? extends NdaPackageValidatorI> clazz : _packageValidators) {
			if (clazz.getSimpleName().equals(simpleName)) {
				try {
					return clazz.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					log.error("Error creating validator instance:");
					log.error(ExceptionUtils.getStackTrace(e));
					throw e;
				}
			}
		}
		log.error("ERROR:  Requested validator not found ({})", simpleName);
		return null;
	}

}
