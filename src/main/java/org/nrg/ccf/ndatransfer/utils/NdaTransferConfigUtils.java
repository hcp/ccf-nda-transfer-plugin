package org.nrg.ccf.ndatransfer.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.constants.PrimaryUsage;
import org.nrg.config.entities.Configuration;
import org.nrg.config.exceptions.ConfigServiceException;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.constants.Scope;
import org.nrg.framework.services.SerializerService;
import org.nrg.xft.security.UserI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class NdaTransferConfigUtils {
	
	private ConfigService _configService;
	private final String CONFIG_TOOL = "ndaTransferPlugin";
	private final String CONFIG_PACKAGE_PATH = "dataPackageDefinitions";
	private final String CONFIG_PACKAGE_REASON = "Configuration updated via REST API";
	private SerializerService _serializerService;
	private final Map<String,List<DataPackageDefinition>> _configCache = new HashMap<>();
	private final TypeReference<List<DataPackageDefinition>> DPD_TYPE_REFERENCE = new TypeReference<List<DataPackageDefinition>>() { };

	@Autowired
	public NdaTransferConfigUtils(ConfigService _configService, SerializerService serializerService) {
		super();
		this._configService = _configService;
		this._serializerService = serializerService;
	}
	
	public DataPackageDefinition getDataPackageInfoClone(String projectID, String packageGroup) {
		for (final DataPackageDefinition dataPackage : getDataPackageDefinitions(projectID, true)) {
			if (dataPackage.getPackageGroup().equals(packageGroup)) {
				return dataPackage.buildClone();
			}
		}
		return null;
	}
	
	public DataPackageDefinition getDataPackageInfo(String projectID, String packageGroup) {
		for (final DataPackageDefinition dataPackage : getDataPackageDefinitions(projectID, true)) {
			if (dataPackage.getPackageGroup().equals(packageGroup)) {
				return dataPackage;
			}
		}
		return null;
	}
	
	public void clearPackageDefinitionCache(String projectID) {
		_configCache.remove(projectID);
	}
	
	public List<DataPackageDefinition> getDataPackageDefinitions(String projectID, boolean useCache) {
		if (!useCache) {
			clearPackageDefinitionCache(projectID);
		}
		if (!_configCache.containsKey(projectID)) {
			final List<DataPackageDefinition> dataPackageDefinitions = new ArrayList<>();
			final Configuration config = _configService.getConfig(CONFIG_TOOL, CONFIG_PACKAGE_PATH, Scope.Project, projectID);
			if (config == null) {
				return dataPackageDefinitions;
			}
			final String configJson = config.getContents();
			if (configJson == null || configJson.length()<1) {
				return dataPackageDefinitions;
			}
			try {
				dataPackageDefinitions.addAll(_serializerService.deserializeJson(configJson, DPD_TYPE_REFERENCE));
			} catch (IOException e) {
				log.error("Couldn't instantiate configuration",e);
			} 
			_configCache.put(projectID, dataPackageDefinitions);
		}
		return _configCache.get(projectID);
	}

	public void setDataPackageDefinitions(String projectId, List<? extends DataPackageDefinition> dataPackageDefinitions, UserI user) {
		try {
			final String packageDefinitionJson = _serializerService.toJson(dataPackageDefinitions);
			_configService.replaceConfig(user.getUsername(), CONFIG_PACKAGE_REASON, CONFIG_TOOL, CONFIG_PACKAGE_PATH,
					packageDefinitionJson, Scope.Project, projectId);
			_configCache.remove(projectId);
		} catch (IOException | ConfigServiceException e) {
			log.error("Error saving configuration",e);
		}
	}

	public List<String> getSubgroupList(String projectId, PrimaryUsage primaryUsage) {
		final List<String> returnList = new ArrayList<>();
		for (final DataPackageDefinition packageInfo : getDataPackageDefinitions(projectId, true)) {
			if (packageInfo.getPrimaryUsage() == null || 
					primaryUsage == PrimaryUsage.BOTH || 
					packageInfo.getPrimaryUsage() == PrimaryUsage.BOTH || 
					primaryUsage == packageInfo.getPrimaryUsage()) {
				returnList.add(packageInfo.getPackageGroup());
			}
		}
		return returnList;
	}

}
