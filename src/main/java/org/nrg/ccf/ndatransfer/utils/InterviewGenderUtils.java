package org.nrg.ccf.ndatransfer.utils;

import org.nrg.ccf.ndaguid.constants.NdaGuidConstants;
import org.nrg.xdat.om.XnatSubjectdata;
import org.springframework.stereotype.Component;

@Component
public class InterviewGenderUtils {
	
	public String getInterviewGender(XnatSubjectdata subj) {
		if (subj == null) {
			return "";
		}
		final Object subjectGender = subj.getFieldByName(NdaGuidConstants.GENDER_FIELD);
		if (subjectGender != null && !subjectGender.toString().isEmpty()) {
			return subjectGender.toString().substring(0,1).toUpperCase();
		}
		return ""; 
	}
	
}
