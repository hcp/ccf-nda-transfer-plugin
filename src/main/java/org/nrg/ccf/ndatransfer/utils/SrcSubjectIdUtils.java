package org.nrg.ccf.ndatransfer.utils;

import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.ccf.ndaguid.constants.NdaGuidConstants;
import org.springframework.stereotype.Component;

@Component
public class SrcSubjectIdUtils {
	
	public String getSrcSubjectId(XnatSubjectdata subject) {
		if (subject == null) {
			return "";
		}
		final Object srcSubjectIdObj = subject.getFieldByName(NdaGuidConstants.SRC_SUBJECT_ID_FIELD);
		return (srcSubjectIdObj!=null && srcSubjectIdObj.toString().trim().length()>0) ? srcSubjectIdObj.toString() : subject.getLabel();
	}
	
}
