package org.nrg.ccf.ndatransfer.utils;

import org.nrg.ccf.ndaguid.constants.NdaGuidConstants;
import org.nrg.xdat.model.XnatExperimentdataI;
import org.nrg.xdat.om.base.BaseXnatExperimentdata;
import org.springframework.stereotype.Component;

@Component
public class InterviewAgeUtils {
	
	public String getInterviewAge(XnatExperimentdataI exp) {
		if (!(exp instanceof BaseXnatExperimentdata)) {
			return "";
		}
		final BaseXnatExperimentdata expData = (BaseXnatExperimentdata)exp;
		final Object ndaInterviewAgeObj = expData.getFieldByName(NdaGuidConstants.INTERVIEW_AGE_FIELD);
		return (ndaInterviewAgeObj!=null) ? ndaInterviewAgeObj.toString() : "";
	}
	
}
