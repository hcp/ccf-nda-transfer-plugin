package org.nrg.ccf.ndatransfer.utils;

import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.ccf.ndaguid.constants.NdaGuidConstants;
import org.springframework.stereotype.Component;

@Component
public class GuidUtils {
	
	public String getGuidForSubject(XnatSubjectdata subject) {
		if (subject == null) {
			return "";
		}
		final Object ndaGuidObj = subject.getFieldByName(NdaGuidConstants.GUID_FIELD);
		return (ndaGuidObj!=null) ? ndaGuidObj.toString() : "";
	}
	
}
