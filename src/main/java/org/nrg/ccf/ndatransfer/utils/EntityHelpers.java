package org.nrg.ccf.ndatransfer.utils;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.anno.FileMatcher;
import org.nrg.ccf.ndatransfer.anno.FileSelector;
import org.nrg.ccf.ndatransfer.anno.NdaPackageDefinition;
import org.nrg.ccf.ndatransfer.entities.EntityConfigs;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;
import org.nrg.ccf.ndatransfer.interfce.FileSelectorI;
import org.nrg.framework.utilities.BasicXnatResourceLocator;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class EntityHelpers {
	
	
	private List<Resource> _resourceList;
	final private List<Class<? extends DataPackageDefinition>> _dataPackageDefinitions = new ArrayList<>();
	final private List<Class<? extends FileSelectorI>> _fileSelectors = new ArrayList<>();
	final private List<Class<? extends FileMatcherI>> _fileMatchers = new ArrayList<>();
	final private String[] _componentArr = new String[] { FileMatcher.FILE_MATCHER, FileSelector.FILE_SELECTOR, NdaPackageDefinition.NDA_PACKAGE_DEFINITION };
	
	@SuppressWarnings("unchecked")
	public EntityHelpers() {
		super();
		for (final String component : Arrays.asList(_componentArr)) {
			try {
				for (final Resource resource : getResourceList()) {
					final Properties properties = PropertiesLoaderUtils.loadProperties(resource);
					if (!properties.containsKey(component)) {
						continue;
					}
					final String className = properties.getProperty(component);
					final Class<?> clazz;
					try {
						clazz = Class.forName(className);
					} catch (ClassNotFoundException e) {
						log.debug("ERROR:  Class not found ({})", className);
						continue;
					}
					if (FileSelectorI.class.isAssignableFrom(clazz)) {
						_fileSelectors.add((Class<? extends FileSelectorI>)clazz);
					} else if (DataPackageDefinition.class.isAssignableFrom(clazz)) {
						_dataPackageDefinitions.add((Class<? extends DataPackageDefinition>)clazz);
					} else if (FileMatcherI.class.isAssignableFrom(clazz)) {
						_fileMatchers.add((Class<? extends FileMatcherI>)clazz);
					}
				}
			} catch (IOException e) {
				log.error("Exception thrown", e);
			}
		}
	}
	
	private List<Resource> getResourceList() throws IOException {
		return (_resourceList!=null) ? _resourceList : BasicXnatResourceLocator.getResources("classpath*:META-INF/xnat/ndatransfer/*-ndatransfer.properties");
	}
	
	public EntityConfigs getEntityConfigs() {
		final EntityConfigs entityConfigs = new EntityConfigs();
		for (final Class<? extends DataPackageDefinition> dataPackageDefinitionCl : _dataPackageDefinitions) {
			DataPackageDefinition dataPackageDefinition = null;
			for (final Constructor<?> cons : dataPackageDefinitionCl.getConstructors()) {
				try {
					@SuppressWarnings("unchecked")
					final Constructor<DataPackageDefinition> constructor = (Constructor<DataPackageDefinition>)cons;
					final Object[] nullArr = new Object[constructor.getParameterTypes().length];
					dataPackageDefinition = constructor.newInstance(nullArr);
					if (dataPackageDefinition != null) {
						break;
					}
				} catch (Exception e) {
					log.error("Error obtaining instance -{}", dataPackageDefinitionCl, e);
				}
			}
			if (dataPackageDefinition != null) {
				entityConfigs.getDataPackageDefinitionConfig().put(dataPackageDefinition.getCanonicalClassname(),dataPackageDefinition.configurationYaml());
			}
		}
		for (final Class<? extends FileSelectorI> fileSelectorCl : _fileSelectors) {
			FileSelectorI fileSelector = null;
			for (final Constructor<?> cons : fileSelectorCl.getConstructors()) {
				try {
					@SuppressWarnings("unchecked")
					final Constructor<FileSelectorI> constructor = (Constructor<FileSelectorI>)cons;
					final Object[] nullArr = new Object[constructor.getParameterTypes().length];
					fileSelector = constructor.newInstance(nullArr);
					if (fileSelector!=null) {
							break;
					}
				} catch (Exception e) {
					// Do nothing for now
				}
			}
			if (fileSelector!=null) {
				entityConfigs.getFileSelectorConfig().put(fileSelector.getCanonicalClassname(),fileSelector.configurationYaml());
			}
		}
		for (final Class<? extends FileMatcherI> fileMatcherCl : _fileMatchers) {
			FileMatcherI fileMatcher = null;
			for (final Constructor<?> cons : fileMatcherCl.getConstructors()) {
				try {
					@SuppressWarnings("unchecked")
					final Constructor<FileMatcherI> constructor = (Constructor<FileMatcherI>)cons;
					final Object[] nullArr = new Object[constructor.getParameterTypes().length];
					fileMatcher = constructor.newInstance(nullArr);
					if (fileMatcher!=null) {
							break;
					}
				} catch (Exception e) {
					// Do nothing for now
				}
			}
			if (fileMatcher!=null) {
				entityConfigs.getFileMatcherConfig().put(fileMatcher.getCanonicalClassname(),fileMatcher.configurationYaml());
			}
		}
		return entityConfigs;
	}

}
