package org.nrg.ccf.ndatransfer.utils;

import java.util.ArrayList;
import java.util.List;

import org.nrg.ccf.ndatransfer.abst.ValueInfo;
import org.nrg.ccf.ndatransfer.abst.ValueInfoI;

public class ValueInfoUtils {
	
	public static List<ValueInfo> getValueInfoList(Class<? extends ValueInfoI> ndaEnum) {
		final List<ValueInfo> returnList = new ArrayList<>();
		if (ndaEnum.isEnum()) {
			for (final ValueInfoI value : ndaEnum.getEnumConstants()) {
				returnList.add(((ValueInfoI)value).getValueInfo());
			}
		}
		return returnList;
	}

}
