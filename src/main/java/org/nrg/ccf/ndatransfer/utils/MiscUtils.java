package org.nrg.ccf.ndatransfer.utils;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MiscUtils {
	
	public static String arrayToString(final String[] arr) {
		return listToString(Arrays.asList(arr));
	}
		
	public static String listToString(List<String> list) {
		final Iterator<String> i = list.iterator();
		final StringBuilder sb = new StringBuilder();
		while (i.hasNext()) {
			sb.append(i.next());
			if (i.hasNext()) {
				sb.append(" ");
			}
		}
		return sb.toString();
	}

}
