package org.nrg.ccf.ndatransfer.abst;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class Matcher {
	
	public Object buildClone() {
		try {
			return this.clone();
		} catch (CloneNotSupportedException e) {
			log.error("ERROR:  Couldn't clone object",e);
			return null;
		}
	}

}
