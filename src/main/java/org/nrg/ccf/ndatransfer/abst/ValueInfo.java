package org.nrg.ccf.ndatransfer.abst;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ValueInfo {
	
	private String value;
	private String displayName;

}
