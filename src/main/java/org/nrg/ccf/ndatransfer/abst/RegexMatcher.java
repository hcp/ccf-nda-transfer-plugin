package org.nrg.ccf.ndatransfer.abst;

import java.util.Arrays;

import org.nrg.ccf.ndatransfer.constants.MatchOperator;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class RegexMatcher extends Matcher {
	
	protected String matchRegex;
	protected String excludeRegex;
	protected MatchOperator matchOperator;
	protected Integer matchCount;
	@Setter(AccessLevel.NONE)
	protected String canonicalClassname = this.getClass().getCanonicalName();
	
	public RegexMatcher() {
		super();
		this.matchRegex = "^.*$";
		this.matchOperator = MatchOperator.GREATER_THAN_EQUALS;
		this.matchCount = 0;
	}
	
	public RegexMatcher(String matchRegex, String excludeRegex, MatchOperator matchOperator, Integer matchCount) {
		super();
		this.matchRegex = matchRegex;
		this.excludeRegex = excludeRegex;
		this.matchOperator = matchOperator;
		this.matchCount = matchCount;
	}
	
	protected String regexConfigurationYaml() {
		
		final StringBuilder sb = new StringBuilder();
		
		sb.append("MatchRegex:\n")
		.append("    id: matchRegex\n")
		.append("    name: matchRegex\n")
		.append("    kind: panel.input.textarea\n")
		.append("    label: Match Regex:\n")
		.append("    element:\n")
		.append("        rows: 15\n")
		.append("        style:\n")
		.append("            width: \"100%\"\n")
		.append("    validation: required\n")
		.append("    description: A list of valid regular expressions, one per line, used to match against resource " +
						"labels or file names.  If any expression matches, the item will be selected.\n")
		.append("ExcludeRegex:\n")
		.append("    id: excludeRegex\n")
		.append("    name: excludeRegex\n")
		.append("    kind: panel.input.textarea\n")
		.append("    label: Exclude Regex:\n")
		.append("    element:\n")
		.append("        rows: 15\n")
		.append("        style:\n")
		.append("            width: \"100%\"\n")
		.append("    description: (OPTIONAL)  A list of valid regular expressions, one per line, used to exclude specific " +
						"items from matched results.  If any expression matches, the item will be excluded.\n")
		.append("MatchOperator:\n")
		.append("    id: matchOperator\n")
		.append("    name: matchOperator\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Match Operator:\n")
		.append("    description:  Operator to use when comparing matched results against expected count\n")
		.append("    validation: required\n")
		.append("    options:\n");
		for (final MatchOperator value : MatchOperator.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("MatchCount:\n")
		.append("    id: matchCount\n")
		.append("    name: matchCount\n")
		.append("    kind: panel.input.text\n")
		.append("    label: Expected Count:\n")
		.append("    description:  Expected number of resources returned in a complete session\n")
		.append("    validation: required integer\n")
		;
		
		return sb.toString();
		
	}
	
	public boolean evaluateString(String toBeEval) {
		final String[] matchLines = (getMatchRegex() != null && getMatchRegex().trim().length()>0) ? 
				getMatchRegex().split(System.lineSeparator()) : new String[] {};
		final String[] excludeLines = (getExcludeRegex() != null && getExcludeRegex().trim().length()>0) ? 
				getExcludeRegex().split(System.lineSeparator()) : new String[] {};
		for (final String matchLine : Arrays.asList(matchLines)) {
			if (toBeEval.matches(matchLine)) {
				for (final String excludeLine : Arrays.asList(excludeLines)) {
					if (toBeEval.matches(excludeLine)) {
						return false;
					}
				}
				return true;
			}
			
		}
		return false;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((canonicalClassname == null) ? 0 : canonicalClassname.hashCode());
		result = prime * result + ((excludeRegex == null) ? 0 : excludeRegex.hashCode());
		result = prime * result + ((matchCount == null) ? 0 : matchCount.hashCode());
		result = prime * result + ((matchOperator == null) ? 0 : matchOperator.hashCode());
		result = prime * result + ((matchRegex == null) ? 0 : matchRegex.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegexMatcher other = (RegexMatcher) obj;
		if (canonicalClassname == null) {
			if (other.canonicalClassname != null)
				return false;
		} else if (!canonicalClassname.equals(other.canonicalClassname))
			return false;
		if (excludeRegex == null) {
			if (other.excludeRegex != null)
				return false;
		} else if (!excludeRegex.equals(other.excludeRegex))
			return false;
		if (matchCount == null) {
			if (other.matchCount != null)
				return false;
		} else if (!matchCount.equals(other.matchCount))
			return false;
		if (matchOperator != other.matchOperator)
			return false;
		if (matchRegex == null) {
			if (other.matchRegex != null)
				return false;
		} else if (!matchRegex.equals(other.matchRegex))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return canonicalClassname + " [ matchRegex=" + ((matchRegex.length()<40) ? matchRegex : matchRegex.substring(0,40) + "....") + " ]";
	}

}
