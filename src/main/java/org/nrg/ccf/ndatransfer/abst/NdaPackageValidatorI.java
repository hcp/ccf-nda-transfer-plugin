package org.nrg.ccf.ndatransfer.abst;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xft.security.UserI;

public interface NdaPackageValidatorI extends PcpHelperI {
	
	void validate(PcpStatusEntity statusEntity, UserI user, DataPackageDefinition dataPackageInfo);

}
