package org.nrg.ccf.ndatransfer.abst;

public interface ValueInfoI {
	
	public String getDisplayName();
	
	public ValueInfo getValueInfo();

}
