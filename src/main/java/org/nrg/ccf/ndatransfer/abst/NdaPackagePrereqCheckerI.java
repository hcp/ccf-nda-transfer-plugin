package org.nrg.ccf.ndatransfer.abst;

import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xft.security.UserI;

public interface NdaPackagePrereqCheckerI extends PcpHelperI {
	
	void checkPrereqs(PcpStatusEntity statusEntity, UserI user, DataPackageDefinition dataPackageInfo);

}
