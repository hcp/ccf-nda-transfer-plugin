package org.nrg.ccf.ndatransfer.abst;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.validation.constraints.NotNull;

import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.nrg.ccf.ndatransfer.client.entities.ManifestEntry;
import org.nrg.ccf.ndatransfer.client.entities.ManifestFileContents;
import org.nrg.ccf.ndatransfer.constants.ProcessingType;
import org.nrg.ccf.ndatransfer.exception.SubmissionException;
import org.nrg.ccf.ndatransfer.interfce.ExperimentFileSelectorI;
import org.nrg.ccf.ndatransfer.interfce.FileSelectorI;
import org.nrg.ccf.ndatransfer.interfce.SingleScanFileSelectorI;
import org.nrg.ccf.ndatransfer.pojo.PackageStats;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.turbine.utils.ArcSpecManager;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public abstract class ManifestPackageDefinition extends DataPackageDefinition {

	protected transient Gson _gson = new Gson();
	final private List<FileSelectorI> fileSelectors = new ArrayList<>(); 

	@Override
	public List<String> configurationYaml() {

		final List<String> returnList = super.configurationYaml();
		final StringBuilder sb = new StringBuilder();
		sb.append("FileSelectors:\n")
		.append("    id: fileSelectors\n")
		.append("    name: fileSelectors\n")
		.append("    kind: panel.display\n")
		.append("    label: FileSelectors:\n")
		.append("FileSelectorDiv:\n");
		returnList.add(sb.toString());
		return returnList;

	}

	@Override
	public List<String> generateMetadataFileRecords(PcpStatusEntity entity, File manifestDir, File fileDir,
			ProcessingType processingType, Map<String, String> parameters, UserI user) {
		final XnatImagesessiondata session = XnatImagesessiondata.getXnatImagesessiondatasById(entity.getEntityId(), user, false);
		if (session == null) {
			log.error("Couldn't retrieve session:  {}", entity.getEntityLabel());
			return ImmutableList.of();
		}
		try {
			final File manifestFile = this.generateManifestFileAndPopulateFileDirectory(entity, session, manifestDir, fileDir, 
					processingType, parameters, user);
			return ImmutableList.of(buildMetadataRecord(entity, session, manifestFile, null));
		} catch (IOException | SubmissionException e) {
			log.error("Exception thrown populating manifest or generating metadata record:  ",e);
			return ImmutableList.of();
		}
	}

	protected abstract String buildMetadataRecord(PcpStatusEntity entity, XnatImagesessiondata session, File manifestFile, Object recordInfo);


	public PackageStats computePackageStats(PcpStatusEntity entity, UserI user) {

		final XnatImagesessiondata session = XnatImagesessiondata.getXnatImagesessiondatasById(entity.getEntityId(), user, false);
		final String projectId = entity.getProject();
		return computePackageStats(getFileMap(user, session, null, false), projectId);
	}

	private PackageStats computePackageStats(Map<String, List<File>> fileMap, String projectId) {
		final String fileDir = ".";
		final Map<String, File> alreadyMapped = new HashMap<>();
		for (final Entry<String, List<File>> mappedFiles : fileMap.entrySet()) {
			final String linkedF = fileDir + "/" + mappedFiles.getKey();
			final List<File> targetFiles = mappedFiles.getValue();
			for (final File target : targetFiles) {
				if (target.exists()) {
					if (!alreadyMapped.containsKey(linkedF)) {
						alreadyMapped.put(linkedF, target);
					} else {
						final long targetModified = target.lastModified();
						final File currentFile = alreadyMapped.get(linkedF);
						final long currentModified = currentFile.lastModified();
						if (targetModified>currentModified) {
							alreadyMapped.put(linkedF, target);
						}
					}
				} 
			}
		}
		long packageSize = 0;
		for (final File f : alreadyMapped.values()) {
			packageSize = packageSize + f.length();
		}
		final PackageStats stats = new PackageStats();
		stats.setPackageSize(packageSize);
		return stats;
	}

	public Map<String, List<File>> getFileMap(
		UserI user,
		XnatImagesessiondata imgSession,
		XnatImagescandata imgScan)
	{
		return getFileMap(user, imgSession, imgScan, true);
	}

	public Map<String, List<File>> getFileMap(
		@NotNull UserI user,
		@NotNull XnatImagesessiondata imgSession,
		XnatImagescandata imgScan,
		@NotNull Boolean isStrict)
	{
		Map<String, List<File>> rtValue = new LinkedHashMap<>();

		for (FileSelectorI fSlr : getFileSelectors())
		{
			if (fSlr instanceof ExperimentFileSelectorI)
			{
				rtValue.putAll(((ExperimentFileSelectorI)fSlr).generateFileMap(this, imgSession, user));
			}
			// Can't process single scan if no scan passed.
			else if (imgScan != null && fSlr instanceof SingleScanFileSelectorI)
			{
				rtValue.putAll(((SingleScanFileSelectorI)fSlr).generateFileMap(this, imgSession, imgScan, user));
			}
			// If no image scan data specified, we attempt
			// to get all data related file maps.
			else if (imgScan == null && fSlr instanceof SingleScanFileSelectorI && !isStrict)
			{
				for (final XnatImagescandataI scan : imgSession.getScans_scan()) {
					rtValue.putAll(((SingleScanFileSelectorI)fSlr).generateFileMap(this, imgSession, scan, user));
				}
			}
		}

		return rtValue;
	}

	public File generateManifestFileAndPopulateFileDirectory(PcpStatusEntity entity, XnatImagesessiondata session, XnatImagescandata scan, 
			File manifestDir, File fileDir, ProcessingType processingType, 
			Map<String, String> parameters, UserI user) throws IOException, SubmissionException {
		 
		final String infoChunk = (scan != null) ? "_" + scan.getSeriesDescription().replaceAll("[_.]", "") : "";
		final File manifestFile = new File(manifestDir,entity.getEntityLabel() + "_" + entity.getSubGroup() + infoChunk + "_manifest.json");
		final String projectId = entity.getProject();
		manifestFile.createNewFile();
		final Map<String,List<File>> fileMap = getFileMap(user, session, scan);
		final String regenerateDuplicatesStr = parameters.get("NdaTransferSubmitter-regenerate-duplicates");
		Boolean regenerateDuplicates = false;
		try {
			regenerateDuplicates = Boolean.valueOf(regenerateDuplicatesStr);
		} catch (Exception e) { 
			// Do nothing 
		}
		if (regenerateDuplicates) {
			removeSymlinksForDuplicatedFiles(entity, fileDir, fileMap);
		}
		generateSymlinks(fileMap, fileDir, projectId, processingType);
		populateManifest(fileMap, fileDir, manifestFile, processingType);
		return manifestFile;
		
	}

	public File generateManifestFileAndPopulateFileDirectory(PcpStatusEntity entity, XnatImagesessiondata session, File manifestDir, File fileDir, 
			ProcessingType processingType, Map<String, String> parameters, UserI user) throws IOException, SubmissionException {
		
		return generateManifestFileAndPopulateFileDirectory(entity, session, null, manifestDir, fileDir, processingType, parameters, user);
		
	}
	
	public void addFileSelector(FileSelectorI fileSelector) {
		if (!fileSelectors.contains(fileSelector)) {
			this.fileSelectors.add(fileSelector);
		}
	}
	
	class ValueComparator implements Comparator<String> {
			
		private Map<String, List<File>> _map;

		public ValueComparator(Map<String, List<File>> map) {
			super();
			_map = map;
		}

		@Override
		public int compare(String ins1, String ins2) {
			final List<File> f1 = _map.get(ins1);
			final List<File> f2 = _map.get(ins2);
			if (f1 == null || f2 == null || f1.isEmpty() || f2.isEmpty()) {
				return ins1.compareTo(ins2);
			}
			return f1.get(0).getPath().compareTo(f2.get(0).getPath());
		}
			
	}
	
	private void removeSymlinksForDuplicatedFiles(PcpStatusEntity entity, File fileDir, Map<String, List<File>> fileMap) {
		final File entityDir = new File(fileDir, entity.getEntityLabel());
		if (!(entityDir.exists() && entityDir.isDirectory())) {
			return;
		}
		final Collection<File> fileList = FileUtils.listFiles(entityDir, new RegexFileFilter("^.*__[0-9].*$"), TrueFileFilter.INSTANCE);
		for (final File fl : fileList) {
			final Path flPath = Paths.get(fl.toURI());
			if (!Files.isSymbolicLink(flPath)) {
				continue;
			}
			try {
				final Path linkedFl = Files.readSymbolicLink(flPath);
				// We only want to delete links if they're part of the current filemap.
				boolean hasMatch = false;
				for (String mappedF : fileMap.keySet()) {
					if (linkedFl.toString().endsWith(mappedF) || fl.getPath().replaceFirst("__[0-9]*", "").endsWith(mappedF)) {
						hasMatch = true;
						break;
					}
				}
				if ((!hasMatch) || linkedFl.getFileName().equals(flPath.getFileName())) {
					continue;
				}
				FileUtils.deleteQuietly(fl);
			} catch (IOException e) {
				// Do nothing
			}
		}
	}
	
	private void generateSymlinks(Map<String, List<File>> fileMap, File fileDir, String projectId, ProcessingType processingType) {
		final String archiveLoc = ArcSpecManager.GetInstance().getArchivePathForProject(projectId);
		final Path archivePath = Paths.get(archiveLoc);
		final Path archiveParent = archivePath.getParent();
		// Let's create a sorted map to increase the likelihood that incremented duplicate files from different folders 
		// get incremented together (e.g. the various batch txt files and logs from the pipelines container)
		final Comparator<String> comp = new ValueComparator(fileMap);
		final TreeMap<String, List<File>> sortedMap = new TreeMap<String, List<File>>(comp);
		sortedMap.putAll(fileMap);
		// sortedMap and fileMap should always be the same size, but I've seen a but whereby not all entries were added to
		// the sortedMap.  Let's go ahead and check for this and use the non-sorted map if the sorted one is missing any entries.
		for (final Entry<String, List<File>> mappedFiles : (sortedMap.size()>=fileMap.size()) ? sortedMap.entrySet() : fileMap.entrySet()) {
			final File linkedF = new File(fileDir, mappedFiles.getKey());
			linkedF.getParentFile().mkdirs();
			final List<File> targetFiles = mappedFiles.getValue();
			Collections.sort(targetFiles, new Comparator<File>() {

				@Override
				public int compare(File f1, File f2) {
					return f1.getPath().compareTo(f2.getPath());
				}
				
			});
			for (final File target : targetFiles) {
				if (target.exists()) {
					final Path linkedFP = linkedF.toPath();
				    final Path targetP = target.toPath();
					if (!linkedF.exists()) {
						try {
							createLink(linkedFP, targetP, archiveParent);
						} catch (IOException e) {
							log.warn("WARNING:  could not create symbolic link for manifest file:  {}", target.getPath(), e);
						}
					} else if (isArchiveLinkAndNeedsRelativized(linkedFP, targetP, archiveParent)) {
						if (!Files.isSymbolicLink(linkedFP)) {
							continue;
						}
						log.debug("NEEDS RELATIVIZED:  linkedFP=" + linkedFP + ", targetP=" + targetP +", archiveParent=" + archiveParent + ")");
						FileUtils.deleteQuietly(linkedF);
						try {
							createLink(linkedFP, targetP, archiveParent);
						} catch (IOException e) {
							log.warn("WARNING:  could not create symbolic link for manifest file:  {}", target.getPath(), e);
						}
					} else {
						// If the file exists, we'll replace the existing link if the current file is more recent, 
						// keep up the original file under an incremented filename. If the current file is older, 
						// we'll create an incremented link.
						if (!Files.isSymbolicLink(linkedFP)) {
							continue;
						}
						final long targetModified = target.lastModified();
						try {
							Path currentPath = Files.readSymbolicLink(linkedFP);
							if (currentPath.equals(targetP)) {
								continue;
							}
							// When we have a relative path, we need to append it to the parent of the current file.  Then
							// we need to compare canonical paths of the files.  The path will not be equal.
							// We could probably make the assumption here that we could just subtitute the archivePath
							// for the relative part, but just in case.......
							if (currentPath.startsWith("..")) {
								currentPath = Paths.get(linkedFP.getParent().toString(),currentPath.toString()); 
								try {
									if (currentPath.toFile().getCanonicalPath().equals(targetP.toFile().getCanonicalPath())) {
										continue;
									}
								} catch (Exception e) {
									if (currentPath.toFile().getAbsolutePath().equals(targetP.toFile().getAbsolutePath())) {
										continue;
									}
								}
							}
							final File currentFile = currentPath.toFile();
							final long currentModified = currentFile.lastModified();
							if (processingType == ProcessingType.CINAB_ARCHIVE || processingType == ProcessingType.CINAB_STRUCTURE) {
								if (targetModified>currentModified) {
									// More recent files.  Replace the current symlink with this one.
									try {
										moveExistingFile(linkedF, currentFile);
									} catch (IOException e2) {
										log.error("ERROR: thrown moving file to replace symlink for duplicate filename. ", e2);
										continue;
									}
									createLink(linkedFP, targetP, archiveParent);
								} else {
									// Less recent file.  Create incremented link.
									createIncrementedLink(linkedFP, targetP, archiveParent);
								}
							} else {
								if (targetModified>currentModified) {
									FileUtils.deleteQuietly(linkedF);
									createLink(linkedFP, targetP, archiveParent);
								}
							}
						} catch (IOException e) {
							log.warn("WARNING:  could not create symbolic link for manifest file:  {}", target.getPath(), e);
						}
					}
				} else {
					log.warn("WARNING:  manifest file does not exist:  {}", target.getPath());
				}
			}
		}
		return;
	}

	private boolean isArchiveLinkAndNeedsRelativized(Path linkedFP, Path targetP, Path archiveParent) {
	    if (!(linkedFP.startsWith(archiveParent) && targetP.startsWith(archiveParent))) {
	    	// Do nothing.  Return later.
	    } else {
	    	try {
				final Path currentPath = Files.readSymbolicLink(linkedFP);
				if (currentPath.startsWith(archiveParent)) {
					log.debug("currentPath=" + currentPath);
					return true;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    return false;
	}

	private void createLink(Path linkedFP, Path targetP, Path archiveParent) throws IOException {
	    // If we're creating symlinks from the archive to the archive, let's create relative links.
	    if (!(linkedFP.startsWith(archiveParent) && targetP.startsWith(archiveParent))) {
	    	Files.createSymbolicLink(linkedFP, targetP);
	    } else {
	    	final Path relTargetP = linkedFP.getParent().relativize(targetP);
	    	Files.createSymbolicLink(linkedFP, relTargetP);
	    }
	}

	private void moveExistingFile(File linkedF, File currentFile) throws IOException {
		for (int i=1; i<=20; i++) {
			final String destName = getIncrementedFileName(linkedF, i);
			final File destFile = new File(linkedF.getParent(), destName);
			if (!destFile.exists()) {
				FileUtils.moveFile(linkedF, destFile);
				return;
			} else {
				final Path destPath = destFile.toPath();
				if (Files.isSymbolicLink(destPath)) {
					final Path destFileLinkPath = Files.readSymbolicLink(destPath);
					if (destFileLinkPath.equals(currentFile.toPath())) {
						return;
					}
				}
			}
		}
	}

	private void createIncrementedLink(Path linkedFP, Path targetP, Path archiveParent) throws IOException {
		final File linkedFPfile = linkedFP.toFile();
		for (int i=1; i<=20; i++) {
			final String destName = getIncrementedFileName(linkedFPfile, i);
			final File destFile = new File(linkedFPfile.getParent(), destName);
			if (!destFile.exists()) {
				createLink(destFile.toPath(), targetP, archiveParent);
				return;
			} else {
				final Path destPath = destFile.toPath();
				if (Files.isSymbolicLink(destPath)) {
					// Make sure we don't already have a file for this one.
					final Path destFileLinkPath = Files.readSymbolicLink(destPath);
					if (destFileLinkPath.equals(targetP)) {
						return;
					}
				}
			}
		}
	}

	private String getIncrementedFileName(File currentFile, int i) {
		final String currentName = currentFile.getName();
		int inx = currentName.lastIndexOf(".");
		if (inx > 0) {
			final StringBuilder sb = new StringBuilder();
			sb.append(currentName.substring(0, inx));
			sb.append("__");
			sb.append(i);
			sb.append(currentName.substring(inx));
			return sb.toString();
		} else {
			return currentName + "__" + i;
		}
	}

	private void populateManifest(Map<String, List<File>> fileMap, File fileDir, File manifestFile, ProcessingType processingType) throws SubmissionException {
		final List<ManifestEntry> manifestEntries = new ArrayList<>();
		for (final Entry<String, List<File>> mappedFiles : fileMap.entrySet()) {
			for (final File mappedFile : mappedFiles.getValue()) {
				boolean computeMd5 = !(processingType == ProcessingType.CINAB_ARCHIVE || processingType == ProcessingType.CINAB_STRUCTURE);
				final ManifestEntry manifestEntry = new ManifestEntry(mappedFiles.getKey(),mappedFile, computeMd5); 
				if (!manifestEntries.contains(manifestEntry)) {
					manifestEntries.add(manifestEntry);
				}
			}
		}
		Collections.sort(manifestEntries, new Comparator<ManifestEntry>() {

			@Override
			public int compare(ManifestEntry m1, ManifestEntry m2) {
				return m1.getPath().compareTo(m2.getPath());
			}
			
		});
		try {
			FileUtils.write(manifestFile, _gson.toJson(new ManifestFileContents(manifestEntries)));
		} catch (IOException e) {
			throw new SubmissionException("ERROR:  Couldn't populate manifest (" + manifestFile.getName() + ")",e);
		}
	}
	
	public void initializeTransientFields() {
		super.initializeTransientFields();
		_gson = new GsonBuilder().setPrettyPrinting().create();
	}

}
