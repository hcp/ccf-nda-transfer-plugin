package org.nrg.ccf.ndatransfer.abst;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.ndatransfer.constants.NdaManifestType;
import org.nrg.ccf.ndatransfer.constants.PrimaryUsage;
import org.nrg.ccf.ndatransfer.constants.ProcessingType;
import org.nrg.ccf.ndatransfer.json.DataPackageDefinitionDeserializer;
import org.nrg.ccf.ndatransfer.utils.GuidUtils;
import org.nrg.ccf.ndatransfer.utils.InterviewAgeUtils;
import org.nrg.ccf.ndatransfer.utils.InterviewDateUtils;
import org.nrg.ccf.ndatransfer.utils.InterviewGenderUtils;
import org.nrg.ccf.ndatransfer.utils.PcpHelpers;
import org.nrg.ccf.ndatransfer.utils.SrcSubjectIdUtils;
import org.nrg.ccf.pcp.entities.PcpStatusEntity;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xft.security.UserI;
import org.python.jline.internal.Log;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Getter;
import lombok.Setter;
import lombok.AccessLevel;

@Getter
@Setter
@JsonDeserialize(using = DataPackageDefinitionDeserializer.class)
public abstract class DataPackageDefinition implements Cloneable {
	
	//@Setter(AccessLevel.NONE)
	//protected final Map<String, String> ndaFieldMap = new LinkedHashMap<>();
	@Setter(AccessLevel.NONE)
	protected NdaDataTypeI dataType; 
	
	@Setter(AccessLevel.NONE)
	protected final String canonicalClassname = this.getClass().getCanonicalName();
	
	protected String packageGroup; 
	protected String packageDescription; 
	protected String packagePrereqChecker; 
	protected String packageValidator; 
	protected PrimaryUsage primaryUsage; 
	protected String excludedEntities; 
	
	protected Map<String,String> configurables = new HashMap<>();
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.PROTECTED)
	protected transient GuidUtils _guidUtils;
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.PROTECTED)
	protected transient SrcSubjectIdUtils _srcSubjectIdUtils;
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.PROTECTED)
	protected transient InterviewDateUtils _dateUtils;
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.PROTECTED)
	protected transient InterviewAgeUtils _ageUtils;
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.PROTECTED)
	protected transient InterviewGenderUtils _genderUtils;

	public DataPackageDefinition() {
		super();
		initializeTransientFields();
	}
	
	public List<String> configurationYaml() {
		
		final PcpHelpers pcpHelpers = XDAT.getContextService().getBean(PcpHelpers.class);
		final List<String> returnList = new ArrayList<>();
		final StringBuilder sb = new StringBuilder();
		sb.append("DataType:\n")
		.append("    id: dataType\n")
		.append("    name: dataType\n")
		.append("    kind: panel.select.single\n")
		.append("    label: NDA DataType:\n")
		.append("    validation: required\n")
		.append("    value: ")
		.append(dataType)
		.append("\n")
		.append("    element:\n")
		.append("        disabled: true:\n")
		.append("    options:\n");
		for (final NdaManifestType value : NdaManifestType.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("PackageGroup:\n")
		.append("    id: packageGroup\n")
		.append("    name: packageGroup\n")
		.append("    kind: panel.input.text\n")
		.append("    label: PCP SubGroup (Package ID):\n")
		.append("    validation: required\n")
		.append("    description: Pipeline Control Panel:  Subgroup Value/Package ID\n");
		sb.append("PackageDescription:\n")
		.append("    id: packageDescription\n")
		.append("    name: packageDescription\n")
		.append("    kind: panel.input.text\n")
		.append("    label: Package Description:\n")
		.append("    validation: required\n")
		.append("    description: Package Description\n");
		sb.append("PrimaryUsage:\n")
		.append("    id: primaryUsage\n")
		.append("    name: primaryUsage\n")
		.append("    kind: panel.select.single\n")
		.append("    label: Primary Usage:\n")
		.append("    validation: required\n")
		.append("    description: Primary Usage for Package\n")
		.append("    value: TRANSFERS\n")
		.append("    options:\n");
		for (final PrimaryUsage value : PrimaryUsage.values()) {
			final ValueInfo valueInfo = value.getValueInfo();
			sb.append("        \"");
			sb.append(valueInfo.getValue());
			sb.append("\": \"");
			sb.append(valueInfo.getDisplayName());
			sb.append("\"\n");
		}
		sb.append("PackagePrereqChecker:\n")
		.append("    id: packagePrereqChecker\n")
		.append("    name: packagePrereqChecker\n")
		.append("    className: 'pcpconfig-ele'\n")
		.append("    kind: panel.select.single\n")
		.append("    label: PackagePrereqChecker:\n")
		.append("    validation: required\n");
		final List<Class<? extends NdaPackagePrereqCheckerI>> pcClasses = pcpHelpers.getPackagePrereqCheckerClasses();
		if (pcClasses.size()==1) {
			sb.append("    value: ");
			sb.append(pcClasses.get(0).getCanonicalName());
			sb.append("\n");
		}
		sb.append("    options: \n");
		for (final Class<? extends NdaPackagePrereqCheckerI> clazz : pcClasses) {
			sb.append("        \"");
			sb.append(clazz.getCanonicalName());
			sb.append("\": \"");
			sb.append(clazz.getSimpleName());
			sb.append("\"\n");
		}
		sb.append("PackageValidator:\n")
		.append("    id: packageValidator\n")
		.append("    name: packageValidator\n")
		.append("    className: 'pcpconfig-ele'\n")
		.append("    kind: panel.select.single\n")
		.append("    label: PackageValidator:\n")
		.append("    validation: required\n");
		final List<Class<? extends NdaPackageValidatorI>> vClasses = pcpHelpers.getPackageValidatorClasses();
		if (vClasses.size()==1) {
			sb.append("    value: ");
			sb.append(vClasses.get(0).getCanonicalName());
			sb.append("\n");
		}
		sb.append("    options: \n");
		for (final Class<? extends NdaPackageValidatorI> clazz : vClasses) {
			sb.append("        \"");
			sb.append(clazz.getCanonicalName());
			sb.append("\": \"");
			sb.append(clazz.getSimpleName());
			sb.append("\"\n");
		}
		sb.append("ExcludedEntities:\n")
		.append("    id: excludedEntities\n")
		.append("    name: excludedEntities\n")
		.append("    kind: panel.textarea\n")
		.append("    label: Excluded entities:\n")
		.append("    description: List of entities to be exluded from transfers for this package.  " +
				"These can be listed as [subgroup,subgroup2]:[entity] " + 
				"if entities should only be excluded for certain subgroups.\n");
		returnList.add(sb.toString());
		return returnList;
		
	};
	
	public abstract String generateMetadataFileHeader();
	
	public abstract List<String> generateMetadataFileRecords(PcpStatusEntity entity, File manifestDir, File fileDir, 
			ProcessingType processingType, Map<String, String> parameters, UserI user);
	
	public void safeAppend(final StringBuilder sb, String str) {
		if (sb == null || str == null || str.length()==0) { 
			return;
		}
		str = str.replaceAll(System.lineSeparator(), " ");
		if (!(str.contains("\"") || str.contains(","))) {
			sb.append(str);
		} else if (str.startsWith("\"") && str.endsWith("\"")) {
			sb.append(str);
		} else {
			sb.append("\"").append(str.replaceAll("\"","\\\"")).append("\"");
		}
	}
	
	public void initializeTransientFields() {
		
		_guidUtils = XDAT.getContextService().getBean(GuidUtils.class);
		_srcSubjectIdUtils = XDAT.getContextService().getBean(SrcSubjectIdUtils.class);
		_dateUtils = XDAT.getContextService().getBean(InterviewDateUtils.class);
		_ageUtils = XDAT.getContextService().getBean(InterviewAgeUtils.class);
		_genderUtils = XDAT.getContextService().getBean(InterviewGenderUtils.class);
		
	}
	
	public DataPackageDefinition buildClone() {
		try {
			final DataPackageDefinition cloneObj = (DataPackageDefinition)this.clone();
			cloneObj.initializeTransientFields();
			return cloneObj;
		} catch (CloneNotSupportedException e) {
			Log.error("ERROR:  Could not clone object:  ",e);
			return null;
		}
	}

}
