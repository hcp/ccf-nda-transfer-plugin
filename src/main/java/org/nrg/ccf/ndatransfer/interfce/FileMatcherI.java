package org.nrg.ccf.ndatransfer.interfce;

import java.io.File;
import java.util.Collection;
import java.util.List;

public interface FileMatcherI extends Cloneable {
	
	public String getCanonicalClassname();
	
	public List<String> configurationYaml();
	
	public boolean evaluateFile(String resourceRootPath, File file);
	
	public boolean validateFileMatches(Collection<File> files);
	
	public Object buildClone();
	
}
