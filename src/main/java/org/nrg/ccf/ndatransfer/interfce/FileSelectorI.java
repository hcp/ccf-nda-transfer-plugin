package org.nrg.ccf.ndatransfer.interfce;

import java.util.List;

import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xft.security.UserI;


public interface FileSelectorI extends Cloneable {
	
	public String getCanonicalClassname();
	
	public List<String> configurationYaml();
	
	public boolean readyForTransfer(XnatImagesessiondataI exp, UserI user);
	
	public Object buildClone();

}
