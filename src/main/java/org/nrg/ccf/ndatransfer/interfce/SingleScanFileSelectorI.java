package org.nrg.ccf.ndatransfer.interfce;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xft.security.UserI;

public interface SingleScanFileSelectorI extends FileSelectorI {
	
	public Map<String,List<File>> generateFileMap(DataPackageDefinition packageInfo, XnatImagesessiondataI session, XnatImagescandataI scan, UserI user);
	
}
