package org.nrg.ccf.ndatransfer.interfce;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.xdat.model.XnatImagesessiondataI;
import org.nrg.xft.security.UserI;

public interface ExperimentFileSelectorI extends FileSelectorI {
	
	public Map<String,List<File>> generateFileMap(DataPackageDefinition packageInfo, XnatImagesessiondataI exp, UserI user);
	
}
