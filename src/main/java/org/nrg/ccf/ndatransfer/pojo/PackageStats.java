package org.nrg.ccf.ndatransfer.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PackageStats {
	
	private Long packageFiles = 0l;
	private Long packageSize = 0l;

}
