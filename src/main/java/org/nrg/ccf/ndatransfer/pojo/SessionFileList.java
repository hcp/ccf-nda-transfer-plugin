package org.nrg.ccf.ndatransfer.pojo;

import java.io.File;
import java.util.List;

import org.nrg.xdat.om.XnatImagesessiondata;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SessionFileList {
	
	private String projectId;
	private XnatImagesessiondata imageSession;
	private List<File> fileList;

}
