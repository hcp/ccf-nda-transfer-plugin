package org.nrg.ccf.ndatransfer.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class FileInfo {
	
	private String filePath;
	private String fileURI;

}
