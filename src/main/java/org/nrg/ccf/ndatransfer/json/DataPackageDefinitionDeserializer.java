package org.nrg.ccf.ndatransfer.json;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.nrg.ccf.ndatransfer.abst.DataPackageDefinition;
import org.nrg.ccf.ndatransfer.interfce.FileMatcherI;
import org.nrg.ccf.ndatransfer.interfce.FileSelectorI;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Iterators;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataPackageDefinitionDeserializer extends JsonDeserializer<DataPackageDefinition> {
	
	final List<String> SPECIAL_HANDLING_NODES = Arrays.asList(new String[] {"fileSelectors","configurables"});

	@Override
	public DataPackageDefinition deserialize(final JsonParser jp, final DeserializationContext ctx) throws IOException, JsonProcessingException {
		final ObjectCodec oc = jp.getCodec();
		final JsonNode node = oc.readTree(jp);
		if (!node.has("canonicalClassname")) {
			return null;
		}
		final String canonicalClassname = node.get("canonicalClassname").asText();
		try {
			final Class<?> clazz = Class.forName(canonicalClassname);
			final Object instance = clazz.newInstance();
			deserializeForClassWithInstance(node, clazz, instance);
			if (node.has("fileSelectors")) {
				deserializeFileSelectors(node,instance);
			}
			if (node.has("configurables")) {
				deserializeConfigurables(node,instance);
			}
			final Iterator<Entry<String, JsonNode>> fields = node.fields();
			while (fields.hasNext()) {
				Entry<String, JsonNode> field = fields.next();
				ifListHandleDeserialization(field,instance);
			}
			return (DataPackageDefinition) instance;
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			log.error("Couldn't initialize class",e);
			return null;
		}
	}

	private void deserializeForClassWithInstance(final JsonNode node, final Class<?> clazz, final Object instance) throws IllegalAccessException {
		for (final Method method : clazz.getMethods()) {
			final String methodName = method.getName();
			if (!method.getName().startsWith("set")) {
				continue;
			}
			String nodeName = methodName.substring(3);
			nodeName = nodeName.substring(0,1).toLowerCase() + nodeName.substring(1);
			if (!node.has(nodeName)) {
				continue;
			}
			final Class<?>[] parameterTypes = method.getParameterTypes();
			if (parameterTypes.length!=1) {
				continue;
			}
			final String nodeVal = node.get(nodeName).asText();
			final Class<?> parameterType = parameterTypes[0];
			if (parameterType.equals(String.class)) {
				try {
					method.invoke(instance, nodeVal);
				} catch (IllegalArgumentException | InvocationTargetException e) {
					log.warn("Exception invoking method:  ",e);
				}
			} else {
				try {
					final Method valueOfMethod = parameterType.getMethod("valueOf",String.class);
					method.invoke(instance, valueOfMethod.invoke(null, nodeVal));
				} catch (NoSuchMethodException | SecurityException | IllegalArgumentException | InvocationTargetException e) {
					// Do nothing
				}
			}
		}
	}

	private void deserializeFileSelectors(final JsonNode mainNode, final Object mainInstance) {
		final JsonNode fileSelectorsNode = mainNode.get("fileSelectors");
		if (!fileSelectorsNode.isArray()) {
			return;
		}
		final Class<?> mainClass = mainInstance.getClass();
		final Method addFileSelectorMethod;
		try {
			addFileSelectorMethod = mainClass.getMethod("addFileSelector",FileSelectorI.class);
		} catch (NoSuchMethodException | SecurityException e1) {
			log.info("{} instance has no addFileSelector(FileSelectorI) method.", mainInstance.getClass().getName(), e1);
			return;
		}
		if (addFileSelectorMethod == null) {
			log.debug("No addFileSelector method found for class {}.", mainClass.getName());
		}
		for (final JsonNode fileSelectorNode : fileSelectorsNode) {
			if (!fileSelectorNode.has("canonicalClassname")) {
				continue;
			}
			final String canonicalClassname = fileSelectorNode.get("canonicalClassname").asText();
			try {
				final Class<?> clazz = Class.forName(canonicalClassname);
				final Object instance = clazz.newInstance();
				deserializeForClassWithInstance(fileSelectorNode, clazz, instance);
				if (fileSelectorNode.has("fileMatchers")) {
					deserializeFileMatchers(fileSelectorNode,instance);
				}
				try {
					addFileSelectorMethod.invoke(mainInstance, instance);
				} catch (IllegalArgumentException | InvocationTargetException e) {
					log.error("Couldn't add file selector to DataPackageDefinition",e);
				}
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				log.error("Couldn't initialize class",e);
			}
		}
	}

	private void deserializeFileMatchers(final JsonNode selectorNode, final Object selectorInstance) {
		final JsonNode fileMatchersNode = selectorNode.get("fileMatchers");
		if (!fileMatchersNode.isArray()) {
			return;
		}
		final Method addFileMatcherMethod;
		try {
			addFileMatcherMethod = selectorInstance.getClass().getMethod("addFileMatcher",FileMatcherI.class);
		} catch (NoSuchMethodException | SecurityException e1) {
			log.warn("WARNING: {} instance has no addFileMatcher(FileMatcherI) method.", selectorInstance.getClass().getName(), e1);
			return;
		}
		for (final JsonNode fileMatcherNode : fileMatchersNode) {
			if (!fileMatcherNode.has("canonicalClassname")) {
				continue;
			}
			final String canonicalClassname = fileMatcherNode.get("canonicalClassname").asText();
			try {
				final Class<?> clazz = Class.forName(canonicalClassname);
				final Object instance = clazz.newInstance();
				deserializeForClassWithInstance(fileMatcherNode, clazz, instance);
				try {
					addFileMatcherMethod.invoke(selectorInstance, instance);
				} catch (IllegalArgumentException | InvocationTargetException e) {
					log.error("Couldn't add file selector to DataPackageDefinition",e);
				}
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				log.error("Couldn't initialize class",e);
			}
			
		}
	}

	private void deserializeConfigurables(final JsonNode selectorNode, final Object selectorInstance) {
		final JsonNode configurablesNode = selectorNode.get("configurables");
		final Method setConfigurablesMethod;
		try {
			setConfigurablesMethod = selectorInstance.getClass().getMethod("setConfigurables",Map.class);
		} catch (NoSuchMethodException | SecurityException e1) {
			log.warn("WARNING: {} instance has no setConfigurables(Map) method.", selectorInstance.getClass().getName(), e1);
			return;
		}
		final Map<String, String> configurablesMap = new HashMap<>();
		Iterator<Entry<String, JsonNode>> i = configurablesNode.fields();
		while (i.hasNext()) {
			final Entry<String, JsonNode> entry = i.next();
			configurablesMap.put(entry.getKey(), entry.getValue().asText());
		}
		try {
			setConfigurablesMethod.invoke(selectorInstance, configurablesMap);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			log.error("ERROR invoking setConfigurables method");
		}
	}

	private void ifListHandleDeserialization(Entry<String, JsonNode> field, Object selectorInstance) {
		if (SPECIAL_HANDLING_NODES.contains(field.getKey())) {
			return;
		}
		final JsonNode fieldValue = field.getValue();
		Iterator<JsonNode> elements = fieldValue.elements();
		if (Iterators.size(elements)<=1) {
			return;
		}
		final String fieldName = field.getKey();
		final Method setMethod;
		try {
			setMethod = selectorInstance.getClass().getMethod("set" + capitalizeFirst(fieldName),List.class);
		} catch (NoSuchMethodException | SecurityException e1) {
			return;
		}
		final List<String> valueList = new ArrayList<>();
		elements = fieldValue.elements();
		while (elements.hasNext()) {
			final JsonNode element = elements.next();
			valueList.add(element.asText());
		}
		try {
			setMethod.invoke(selectorInstance, valueList);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// Do nothing
		}
	}
	
	public static String capitalizeFirst(final String str) {
	    return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

}
