package org.nrg.ccf.ndatransfer.exception;

public class SubmissionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6474403134971807272L;

	public SubmissionException() {
		super();
	}

	public SubmissionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public SubmissionException(String message, Throwable cause) {
		super(message, cause);
	}

	public SubmissionException(String message) {
		super(message);
	}

	public SubmissionException(Throwable cause) {
		super(cause);
	}

}
