package org.nrg.ccf.ndatransfer.exception;

public class ValidateAndUploadException extends Exception {

	private static final long serialVersionUID = -6814420962112548669L;

	public ValidateAndUploadException() {
	}

	public ValidateAndUploadException(String message) {
		super(message);
	}

	public ValidateAndUploadException(Throwable cause) {
		super(cause);
	}

	public ValidateAndUploadException(String message, Throwable cause) {
		super(message, cause);
	}

	public ValidateAndUploadException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
