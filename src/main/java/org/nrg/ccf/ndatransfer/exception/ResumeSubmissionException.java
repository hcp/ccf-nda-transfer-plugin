package org.nrg.ccf.ndatransfer.exception;

public class ResumeSubmissionException extends Exception {

	private static final long serialVersionUID = -1485069577377217884L;

	public ResumeSubmissionException() {
	}

	public ResumeSubmissionException(String message) {
		super(message);
	}

	public ResumeSubmissionException(Throwable cause) {
		super(cause);
	}

	public ResumeSubmissionException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResumeSubmissionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
