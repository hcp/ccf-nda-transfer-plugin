package org.nrg.ccf.ndatransfer.exception;

public class NdaRestClientException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6409780310535454766L;

	public NdaRestClientException() {
	}

	public NdaRestClientException(String message) {
		super(message);
	}

	public NdaRestClientException(Throwable cause) {
		super(cause);
	}

	public NdaRestClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public NdaRestClientException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
