package org.nrg.ccf.ndatransfer.conf;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(
			value = "ccfNdaTransferPlugin",
			name = "CCF NDA Transfer Plugin",
			entityPackages = "org.nrg.ccf.ndatransfer.entities",
			log4jPropertiesFile = "/META-INF/resources/ndaTransferLog4j.properties"
		)
@ComponentScan({ 
		"org.nrg.ccf.ndatransfer.conf",
		"org.nrg.ccf.ndatransfer.client",
		"org.nrg.ccf.ndatransfer.dao",
		"org.nrg.ccf.ndatransfer.entities",
		"org.nrg.ccf.ndatransfer.services",
		"org.nrg.ccf.ndatransfer.utils",
		"org.nrg.ccf.ndatransfer.xapi"
	})
@Slf4j
public class CcfNdaTransferPlugin {
	
	public CcfNdaTransferPlugin() {
		log.info("Configuring the Intradb NDA Transfer Plugin.");
	}
	
}
